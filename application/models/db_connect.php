<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_connect extends CI_Model {


	public function daftar_barang()
	{
		return $this->db->query("select b.kode_barang, b.kategori, b.merek, b.tipe, b.`size`, b.harga from barang b order by merek ASC;");
	}

	public function input_barang($data)
	{
		return $this->db->insert("barang", $data);
	}

	public function hapus_barang($kode_barang)
	{
		$this->db->where('kode_barang', $kode_barang);
		return $this->db->delete("barang");
	}

	public function get_kode_barang($kode_barang)
	{
		$this->db->where('kode_barang', $kode_barang);
		return $this->db->get("barang");
	}

	public function update_barang_proses($data, $kode_barang)
	{
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update("barang", $data);
	}

	public function lokasi_penyimpanan()
	{
		return  $data = $this->db->get("lokasi_penyimpanan")->result();	
	}

	public function cek_kode_barang($kode_barang)
	{
		$this->db->where("kode_barang", $kode_barang);
		return $this->db->get("barang");
	}

	public function cek_lokasi_barang($kode_barang)
	{
		return $this->db->query("select lp.nama_lokasi_penyimpanan, lb.kode_barang
			from lokasi_barang lb
			left join barang b on b.kode_barang = lb.kode_barang
			left join lokasi_penyimpanan lp on lb.id_lokasi = lp.id_lokasi
			where b.kode_barang = '$kode_barang';");
	}

	public function daftarkan_barang_proses($data, $id_lokasi='', $kode_barang='')
	{
		$cek_data = $this->db->query("select id_lokasi, kode_barang from lokasi_barang where id_lokasi='$id_lokasi' and kode_barang= '$kode_barang'");

		if ($cek_data->num_rows() > 0 ) {

			echo "<h1>Data Sudah ada !</h1>";
			echo "<br>";
			echo "<a href=''><h4>Klik untuk Kembali <<</h4></a>";

		}
		else
		{
			$this->db->insert("lokasi_barang", $data);
			redirect("Traffic_data/daftarkan_barang");
		}
	}

	public function input_barang_masuk_proses($data, $kode_barang='', $id_lokasi='')
	{
		$cek_data = $this->db->query("select id_lokasi, kode_barang from lokasi_barang where id_lokasi='$id_lokasi' and kode_barang= '$kode_barang'");

		if ($cek_data->num_rows() > 0) {
			
			$this->db->insert("barang_masuk", $data);
			redirect("Traffic_data/input_barang_masuk");
		}

		else
		{
			echo "<br><br><br><br><br><br>";
			echo "<center>";
			echo "Barang Belum Terdata Digudang ".$id_lokasi." !";
			echo "<br>";
			echo "Mohon periksa kembali Lokasi barang atau daftarkan barang di lokasi penyimpanan ".$id_lokasi;
			echo "<br>";
			echo "Terima Kasih";
			echo "<br><br><br>";
			echo "<a href='".base_url()."Traffic_data/input_barang_masuk'".">Klik Kembali <<</a>";
			echo "<center>";
		}
	}

	public function data_barang_masuk()
	{
		$month = date('m');
		return $this->db->query("SELECT k.nama, bm.tanggal_masuk_barang, b.kategori, b.merek, b.tipe, 
			bm.banyak_barang, lp.nama_lokasi_penyimpanan FROM barang_masuk bm
			LEFT JOIN barang b ON bm.kode_barang = b.kode_barang
			LEFT JOIN lokasi_penyimpanan lp ON bm.id_lokasi = lp.id_lokasi
			LEFT JOIN karyawan k ON bm.id_karyawan = k.id_karyawan
			WHERE MONTH(bm.tanggal_masuk_barang) = '$month'
			ORDER BY bm.kbarang_masuk DESC
			");
	}

	public function gudang1()
	{
		return $this->db->query("SELECT b.kode_barang, b.kategori, b.merek, b.tipe, b.size, lb.stok_barang FROM barang b
			INNER JOIN lokasi_barang lb ON b.kode_barang = lb.kode_barang
			WHERE lb.id_lokasi ='GD-1'");
	}

	public function gudang2()
	{
		return $this->db->query("SELECT b.kode_barang, b.kategori, b.merek, b.tipe, b.size, lb.stok_barang FROM barang b
			INNER JOIN lokasi_barang lb ON b.kode_barang = lb.kode_barang
			WHERE lb.id_lokasi ='GD-2'");
	}
	public function tkmm()
	{
		return $this->db->query("SELECT b.kode_barang, b.kategori, b.merek, b.tipe, b.size, lb.stok_barang FROM barang b
			INNER JOIN lokasi_barang lb ON b.kode_barang = lb.kode_barang
			WHERE lb.id_lokasi ='TK-MM'");
	}
	public function tkpp()
	{
		return $this->db->query("SELECT b.kode_barang, b.kategori, b.merek, b.tipe, b.size, lb.stok_barang FROM barang b
			INNER JOIN lokasi_barang lb ON b.kode_barang = lb.kode_barang
			WHERE lb.id_lokasi ='TK-PP'");
	}


	public function gudang_cari($id_lokasi)
	{
		return $this->db->query("SELECT b.kode_barang, b.kategori, b.merek, b.tipe, b.size, lb.stok_barang FROM barang b
			INNER JOIN lokasi_barang lb ON b.kode_barang = lb.kode_barang
			WHERE lb.id_lokasi ='$id_lokasi'");
	}

	public function cari_barang($kode_barang= '')
	{
		// return $this->db->query("SELECT b.kategori, b.kode_barang, b.merek, b.tipe, b.harga, lp.nama_lokasi_penyimpanan, lb.stok_barang FROM barang b
		// 	LEFT JOIN lokasi_barang lb ON b.kode_barang = lb.kode_barang
		// 	LEFT JOIN lokasi_penyimpanan lp ON lb.id_lokasi = lp.id_lokasi
		// 	WHERE b.kode_barang like '%$kode_barang%' or b.merek like '%$kode_barang%' or 
		// 	b.tipe like '%$kode_barang%' or b.harga like '%$kode_barang%' or b.kategori like '%$kode_barang%'");

		return $this->db->query("SELECT b.kategori, b.kode_barang, b.merek, b.tipe, b.harga, lp.nama_lokasi_penyimpanan, lb.stok_barang FROM barang b
			LEFT JOIN lokasi_barang lb ON b.kode_barang = lb.kode_barang
			LEFT JOIN lokasi_penyimpanan lp ON lb.id_lokasi = lp.id_lokasi
			WHERE CONCAT(b.kode_barang, b.merek, b.tipe, b.harga, b.kategori) like '%".$kode_barang."%' ");

		// if ($tampil_data->num_rows() > 0) {
		// 	return $tampil_data;
		// }

	}

	public function tanggal_masuk_barang()
	{
		$month = date('m');
		return $this->db->query("SELECT bm.tanggal_masuk_barang FROM barang_masuk bm
			WHERE MONTH(bm.tanggal_masuk_barang) = '$month'
			GROUP BY bm.tanggal_masuk_barang");
	}

	public function daftar_kategori()
	{
		return $this->db->get("kategori");
	}

	public function tambah_kategori($data)
	{
		$this->db->insert("kategori", $data);
		redirect("Traffic_data/menu_kategori");
	}

	public function petugas_lapangan()
	{
		return $this->db->query("SELECT id_karyawan, nama FROM karyawan WHERE posisi = 'Lapangan' or posisi = 'Admin'");
	}

	public function input_barang_keluar_proses($data, $kode_barang='', $id_lokasi = '')
	{
		$cek_data = $this->db->query("select id_lokasi, kode_barang from lokasi_barang where id_lokasi='$id_lokasi' and kode_barang= '$kode_barang'");

		if ($cek_data->num_rows() > 0) {
			
			$this->db->insert("barang_keluar", $data);
			redirect("Traffic_data/input_barang_keluar");
		}
		else
		{

			echo "<br><br><br><br><br><br>";
			echo "<center>";
			echo "Barang Belum Terdata Digudang ".$id_lokasi." !";
			echo "<br>";
			echo "Mohon periksa kembali Lokasi barang atau daftarkan barang di lokasi penyimpanan ".$id_lokasi;
			echo "<br>";
			echo "Terima Kasih";
			echo "<br><br><br>";
			echo "<a href='".base_url()."Traffic_data/input_barang_keluar'".">Klik Kembali <<</a>";
			echo "<center>";
		}

	}

	public function data_barang_keluar()
	{
		$month = date('m');
		return $this->db->query("SELECT bk.petugas_admin, bk.petugas_lapangan, bk.tanggal_keluar_barang, b.kode_barang, 
			bk.banyak_barang, lp.nama_lokasi_penyimpanan, bk.keterangan FROM barang_keluar bk
			LEFT JOIN barang b ON bk.kode_barang = b.kode_barang
			LEFT JOIN lokasi_penyimpanan lp ON bk.id_lokasi = lp.id_lokasi
			WHERE MONTH(bk.tanggal_keluar_barang) = '$month'
			ORDER BY bk.kbarang_keluar DESC
			");
	}

	public function tanggal_keluar_barang()
	{
		$month = date('m');
		return $this->db->query("SELECT bk.tanggal_keluar_barang FROM barang_keluar bk
			WHERE MONTH(bk.tanggal_keluar_barang) = '$month'
			GROUP BY bk.tanggal_keluar_barang");
	}

	public function lokasi_dan_stok($kode_barang)
	{
		return $this->db->query("SELECT lp.nama_lokasi_penyimpanan, lb.stok_barang
			FROM lokasi_barang lb
			LEFT JOIN barang b ON b.kode_barang = lb.kode_barang
			LEFT JOIN lokasi_penyimpanan lp ON lb.id_lokasi = lp.id_lokasi
			WHERE b.kode_barang = '$kode_barang'");
	}


	public function input_mutasi_proses($data, $id_lokasi_pengambilan='', $id_lokasi_penyimpanan='', $kode_barang= '')
	{
		$cek_data = $this->db->query("SELECT id_lokasi, kode_barang FROM lokasi_barang WHERE 
			id_lokasi= '$id_lokasi_pengambilan' OR  id_lokasi = '$id_lokasi_penyimpanan' AND kode_barang = '$kode_barang'
			");

		if ($cek_data->num_rows() > 0) {
			
			$this->db->insert("mutasi_barang", $data);
			redirect("Traffic_data/input_mutasi");
		}
		else
		{

			echo "<br><br><br><br><br><br>";
			echo "<center>";
			echo "Barang Belum Terdata Di penyimpanan".$id_lokasi_pengambilan." maupun di ".$id_lokasi_penyimpanan." !";
			echo "<br>";
			echo "Mohon periksa kembali Lokasi barang atau daftarkan barang";
			echo "<br>";
			echo "Terima Kasih";
			echo "<br><br><br>";
			echo "<a href='".base_url()."Traffic_data/input_mutasi'".">Klik Kembali <<</a>";
			echo "<center>";
		}
	}

	public function daftar_mutasi()
	{
		$month_now = date('m');
		return $this->db->query("SELECT * FROM mutasi_barang WHERE MONTH(tanggal_mutasi) = '$month_now'

			");
		
	}

	public function data_mutasi_barang()
	{
		$month = date('m');
		return $this->db->query("SELECT tanggal_mutasi FROM mutasi_barang WHERE MONTH(tanggal_mutasi) = '$month'
			GROUP BY tanggal_mutasi
			");
	}

	public function tampil_tanggal_mutasi($tanggal_mutasi)
	{
		return $this->db->query("SELECT * FROM mutasi_barang WHERE DATE(tanggal_mutasi) = '$tanggal_mutasi'");
	}

	public function tampil_tanggal_masuk($tanggal_masuk)
	{
		
		return $this->db->query("SELECT bm.kode_barang, bm.tanggal_masuk_barang, bm.banyak_barang, lp.nama_lokasi_penyimpanan, k.nama, bm.keterangan FROM barang_masuk bm
			LEFT JOIN lokasi_penyimpanan lp ON bm.id_lokasi = lp.id_lokasi
			LEFT JOIN karyawan k ON bm.id_karyawan = k.id_karyawan
			WHERE DATE(bm.tanggal_masuk_barang) = '$tanggal_masuk'
			");

	}

	public function tampil_tanggal_keluar($tanggal_keluar)
	{
		return $this->db->query("SELECT bk.petugas_admin, bk.petugas_lapangan, bk.kode_barang, b.kategori, b.merek, b.tipe, bk.tanggal_keluar_barang, bk.banyak_barang, bk.keterangan, lp.nama_lokasi_penyimpanan FROM barang_keluar bk
			LEFT JOIN lokasi_penyimpanan lp ON bk.id_lokasi = lp.id_lokasi
			LEFT JOIN barang b ON bk.kode_barang = b.kode_barang
			WHERE DATE(bk.tanggal_keluar_barang) = '$tanggal_keluar'
			");
	}

	public function download_mutation_ByMonth($month, $year)
	{
		return $this->db->query("SELECT mb.tanggal_mutasi, mb.petugas_admin, mb.petugas_lapangan, mb.id_lokasi_pengambilan, mb.id_lokasi_penyimpanan, mb.kode_barang,
			b.kategori, b.merek, b.tipe, mb.banyak_barang, mb.keterangan FROM mutasi_barang mb
			LEFT JOIN barang b ON mb.kode_barang = b.kode_barang
			WHERE MONTH(mb.tanggal_mutasi) = '$month' AND YEAR(mb.tanggal_mutasi) = '$year'")->result();
	}

	public function download_stok_outByMonth($month, $year)
	{
		return $this->db->query("SELECT bk.tanggal_keluar_barang, bk.petugas_admin, bk.petugas_lapangan,
			lp.nama_lokasi_penyimpanan, bk.kode_barang, b.kategori, b.merek, b.tipe,
			bk.banyak_barang, bk.keterangan 
			FROM barang_keluar bk
			LEFT JOIN lokasi_penyimpanan lp ON bk.id_lokasi = lp.id_lokasi
			LEFT JOIN barang b ON bk.kode_barang = b.kode_barang
			WHERE MONTH(bk.tanggal_keluar_barang) = '$month' AND YEAR(bk.tanggal_keluar_barang) = '$year'")->result();
	}

	public function download_stok_inByMonth($month='', $year='')
	{
		return $this->db->query("SELECT bm.tanggal_masuk_barang, k.nama, b.kode_barang,
			b.kategori, b.merek, b.tipe, bm.banyak_barang,
			bm.keterangan FROM barang_masuk bm
			LEFT JOIN karyawan k ON bm.id_karyawan = k.id_karyawan
			LEFT JOIN barang b ON bm.kode_barang = b.kode_barang
			WHERE MONTH(bm.tanggal_masuk_barang) = '$month' AND YEAR(bm.tanggal_masuk_barang) = '$year' ")->result();
	}

	public function downloadstok($kategori ='')
	{
		if ($kategori == 'all') {
			return $this->db->query("SELECT b.kategori, b.kode_barang, b.merek, b.tipe, b.size, b.harga, sum(lb.stok_barang) as jumlah from barang b
				LEFT JOIN lokasi_barang lb on b.kode_barang = lb.kode_barang
				GROUP BY lb.kode_barang
				ORDER BY b.kategori ASC")->result();		

		}
		else{
			return $this->db->query("SELECT b.kategori, b.kode_barang, b.merek, b.tipe, b.size, b.harga, sum(lb.stok_barang) as jumlah from barang b
				LEFT JOIN lokasi_barang lb on b.kode_barang = lb.kode_barang
				where b.kategori = '$kategori'
				GROUP BY lb.kode_barang
				ORDER BY b.merek ASC
				")->result();	

		}
	}

}

/* End of file  */
/* Location: ./application/models/ */	