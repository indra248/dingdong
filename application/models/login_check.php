<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_check extends CI_Model {


	public function validation($nama_admin='',$password='')
	{
		$this->db->where("nama_admin", $nama_admin);
		$this->db->where("password", $password);
		$query = $this->db->get("karyawan");

		if ($query->num_rows() > 0) 
		{
			
			foreach ($query->result() as $row) {

				$data= array(
					'id_karyawan' => $row->id_karyawan,
					'nama_admin'  => $row->nama_admin,
					'nama' 		  => $row->nama,
					'password'    => $row->password,
					'level_user'  => $row->level_user
				); 
				$this->session->set_userdata($data);
				// echo json_encode($data);

				 redirect("Traffic_data/mainDashboard");
			}
		}
		else{
				redirect("login/");
		}

	}

}

/* End of file login.php */
/* Location: ./application/models/login.php */