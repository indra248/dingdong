<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Security extends CI_Model {


	public function security_check()
	{
		$login= $this->session->userdata('id_karyawan');
		if(empty($login))
		{
			$this->session->sess_destroy();
			redirect("login/deny_access");

		}
		
	}
}

/* End of file security.php */
/* Location: ./application/models/security.php */