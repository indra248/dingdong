
<!DOCTYPE html>
<html>
<head>  
  <title>Formulir Edit</title>  
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.css">
  <script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.js"></script>
  <style type="text/css">
  /* style untuk link popup */
  a.popup-link {
    padding:17px 0;
    text-align: center;
    margin:10% auto;
    position: relative;
    width: 300px;
    color: #fff;
    text-decoration: none;
    background-color: #FFBA00;
    border-radius: 3px;
    box-shadow: 0 5px 0px 0px #eea900;
    display: block;
  }
  a.popup-link:hover {
    background-color: #ff9900;
    box-shadow: 0 3px 0px 0px #eea900;
    -webkit-transition:all 1s;
    transition:all 0.1s;
  }
  /* end link popup*/
  /* animasi popup */

  @-webkit-keyframes autopopup {
    from {opacity: 0;margin-top:-200px;}
    to {opacity: 1;}
  }
  @-moz-keyframes autopopup {
    from {opacity: 0;margin-top:-200px;}
    to {opacity: 1;}
  }
  @keyframes autopopup {
    from {opacity: 0;margin-top:-200px;}
    to {opacity: 1;}
  }
  /* end animasi popup */
  /*style untuk popup */  
  #popup {
    background-color: rgba(0,0,0,0.8);
    position: fixed;
    top:0;
    left:0;
    right:0;
    bottom:0;
    margin:0;
    -webkit-animation:autopopup 1s;
    -moz-animation:autopopup 1s;
    animation:autopopup 1s;
  }
  #popup:target {
    -webkit-transition:all 1s;
    -moz-transition:all 1s;
    transition:all 1s;
    opacity: 0;
    visibility: hidden;
  }

  @media (min-width: 768px){
    .popup-container {
      width:600px;
    }
  }
  @media (max-width: 767px){
    .popup-container {
      width:100%;
    }
  }
  .popup-container {
    position: relative;
    margin:7% auto;
    padding:30px 50px;
    background-color: #fafafa;
    color:#333;
    border-radius: 3px;
  }

  a.popup-close {
    position: absolute;
    top:3px;
    right:3px;
    background-color: #333;
    padding:7px 10px;
    font-size: 20px;
    text-decoration: none;
    line-height: 1;
    color:#fff;
  }
  /* end style popup */

  /* style untuk isi popup */
  .popup-form {
    margin:10px auto;
  }
  .popup-form h2 {
    margin-bottom: 5px;
    font-size: 37px;
    text-transform: uppercase;
  }
  .popup-form .input-group {
    margin:10px auto;
  }
  .popup-form .input-group input {
    padding:17px;
    text-align: center;
    margin-bottom: 10px;
    border-radius:3px;
    font-size: 16px; 
    display: block;
    width: 100%;
  }
  .popup-form .input-group input:focus {
    outline-color:#FB8833; 
  }
  .popup-form .input-group input[type="email"] {
    border:0px;
    position: relative;
  }
  .popup-form .input-group input[type="submit"] {
    background-color: #FB8833;
    color: #fff;
    border: 0;
    cursor: pointer;
  }
  .popup-form .input-group input[type="submit"]:focus {
    box-shadow: inset 0 3px 7px 3px #ea7722;
  }
  /* end style isi popup */

</style>
</head>
<body>

 <?php foreach($update_barang->result() as $row); ?>
 <div class="popup-wrapper" id="popup">
  <div class="popup-container">
    <form action="<?php echo base_url()?>Traffic_data/update_barang_proses/<?php echo $row->kode_barang; ?>" method="POST" class="popup-form">
      <fieldset>
        <div id="legend">
          <legend class=""><b>FORMULIR EDIT</b></legend>
        </div>
        <table cellpadding="3px">

          <tr>
            <td>KODE BARANG</td>
            <td>:</td>
            <td><input type="text" name="kode_barang" value="<?php echo $row->kode_barang;?>" readonly></td>
          </tr>
          <tr>
            <td>KATEGORI</td>
            <td>:</td>
            <td>
              <select name="kategori">
                <option value="<?php echo $row->kategori ?>"><?php echo $row->kategori; ?></option>
                <option value="AC">AC</option>
                <option value="KULKAS">KULKAS</option>
                <option value="TV">TV</option>
                <option value="MESIN CUCI">MESIN CUCI</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>MEREK</td>
            <td>:</td>
            <td><input type="text" name="merek" value="<?php echo $row->merek;?>"></td>
          </tr>
          <tr>
            <td>TIPE</td>
            <td>:</td>
            <td><input type="text" name="tipe" value="<?php echo $row->tipe;?>"></td>
          </tr>
          <tr>
            <td>SIZE</td>
            <td>:</td>
            <td><input type="text" name="size" value="<?php echo $row->size;?>"></td>
          </tr>
          <tr>
            <td>HARGA</td>
            <td>:</td>
            <td><input type="text" name="harga" value="<?php echo $row->harga;?>"></td>
          </tr>
        </table>
      </fieldset>
      <button class="btn btn-primary" type="submit">Simpan</button>
    </form>

    <a class="popup-close" href="#popup">X</a>
  </div>

</body>
</html>