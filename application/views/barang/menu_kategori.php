<!DOCTYPE html>
<html>
<head>
	<title>Kategori</title>
	<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/magopi-logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
	<link href="<?php echo base_url()?>assets/sb-admin/css/sb-admin.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<link href="<?php echo base_url()?>assets/sb-admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/jquery/jquery-ui.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery/jquery-3.3.1.min.js"></script>
	<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->

	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.dataTables.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/dataTables.bootstrap.css">

</head>
<body>


	<body class="fixed-nav sticky-footer bg-dark" id="page-top">
		<!-- Navigation-->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
			<a class="navbar-brand" href="">ADMIN </a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/mainDashboard">
							<i class="fa fa-fw fa fa-desktop"></i>
							<span class="nav-link-text">Home</span>
						</a>
					</li>
					<li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/buku_barang">
							<i class="fa fa-fw fa fa-book"></i>
							<span class="nav-link-text">Buku Barang > Menu Kategori</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_stok">
							<i class="fa fa-fw fa fa-line-chart"></i>
							<span class="nav-link-text">Stok</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Daftar Menu">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_mutasi">
							<i class="fa fa-fw fa fa fa-exchange"></i>
							<span class="nav-link-text">Mutasi</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_barang_masuk">
							<i class="fa fa-fw 	fa fa-toggle-right"></i>
							<span class="nav-link-text">Barang Masuk</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_barang_keluar">
							<i class="fa fa-fw fa fa-toggle-left"></i>
							<span class="nav-link-text">Barang Keluar</span>
						</a>
					</li>
					
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Login/logout">
							<i class="fa fa-fw 	fa fa-user-circle-o"></i>
							<span class="nav-link-text">Logout</span>
						</a>
					</li>
				</ul>
				<ul class="navbar-nav sidenav-toggler">
					<li class="nav-item">
						<a class="nav-link text-center" id="sidenavToggler">
							<i class="fa fa-fw fa-angle-left"></i>
						</a>
					</li>
				</ul>
			</div>
		</nav>
		<div class="content-wrapper">
			<div class="container-fluid">

					<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h6>*<i>Harap pastikan tidak ada kategori yang double.</i></h6>
			</div>

		</div>
		<!-- row -->

				<div class="row">
					<div class="col-md-4">

						<form method="POST" action="<?php echo base_url();?>Traffic_data/tambah_kategori">
							<label>Masukan Kategori baru :</label>
							<table class="table">
								<tr>
									<td><input type="text" class="form-control" name="nama_kategori"></td>
									<td><button type="submit" class="btn btn-success">Tambah</button></td>
								</tr>
							</table>
							
						</form>
					</div>
				</div>


		<div class="row">
			<div class="col-md-6">

				<div class="table-responsive">
					<table class="table table-hover" id="data_barang_masuk">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Kategori</th>
								<th>Opsi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 0; foreach ($daftar_kategori->result() as $row) {$no++;?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $row->nama_kategori; ?></td>
								<td>
									<a href=""><button class="btn btn-warning">Edit</button></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					<script type="text/javascript">
						$(document).ready(function(){
							$('#data_barang_masuk').DataTable();
						});
					</script>
				</div>

			</div>

		</div>


			</div>
			<!-- /.container-fluid-->
		</div>
		<!-- /.content-wrapper-->

		<br>

	




		<footer class="sticky-footer">
			<div class="container">
				<div class="text-center">
					<small>Dingdong</small>
				</div>
			</div>
		</footer>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top">
			<i class="fa fa-angle-up"></i>
		</a>
		<!-- Logout Modal-->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<a class="btn btn-primary" href="login.html">Logout</a>
					</div>
				</div>
			</div>
		</div>	

		<!-- Bootstrap core JavaScript-->
		<!-- <script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery/jquery.min.js"></script> -->
		<script src="<?php echo base_url()?>assets/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<!-- Core plugin JavaScript-->
		<script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>
		<!-- Custom scripts for all pages-->
		<script src="<?php echo base_url()?>assets/sb-admin/js/sb-admin.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
	</div>


</body>
</html>