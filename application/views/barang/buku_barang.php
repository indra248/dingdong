<!DOCTYPE html>
<html>
<head>
	<title>Buku Barang</title>
	<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/magopi-logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
	<link href="<?php echo base_url()?>assets/sb-admin/css/sb-admin.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<link href="<?php echo base_url()?>assets/sb-admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/jquery/jquery-ui.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery/jquery-3.3.1.min.js"></script>
	<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->

	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.dataTables.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/dataTables.bootstrap.css">

</head>
<body>

	<body class="fixed-nav sticky-footer bg-dark" id="page-top">
		<!-- Navigation-->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
			<a class="navbar-brand" href="">ADMIN </a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/mainDashboard">
							<i class="fa fa-fw fa fa-desktop"></i>
							<span class="nav-link-text">Home</span>
						</a>
					</li>
					<li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/buku_barang">
							<i class="fa fa-fw fa fa-book"></i>
							<span class="nav-link-text">Buku Barang</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_stok">
							<i class="fa fa-fw fa fa-line-chart"></i>
							<span class="nav-link-text">Stok</span>
						</a>
						<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Daftar Menu">
							<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_mutasi">
								<i class="fa fa-fw fa fa fa-exchange"></i>
								<span class="nav-link-text">Mutasi</span>
							</a>
						</li>
						<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
							<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_barang_masuk">
								<i class="fa fa-fw 	fa fa-toggle-right"></i>
								<span class="nav-link-text">Barang Masuk</span>
							</a>
						</li>
						<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
							<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_barang_keluar">
								<i class="fa fa-fw fa fa-toggle-left"></i>
								<span class="nav-link-text">Barang Keluar</span>
							</a>
						</li>

						<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
							<a class="nav-link" href="<?php echo base_url();?>Login/logout">
								<i class="fa fa-fw 	fa fa-user-circle-o"></i>
								<span class="nav-link-text">Logout</span>
							</a>
						</li>
					</ul>
					<ul class="navbar-nav sidenav-toggler">
						<li class="nav-item">
							<a class="nav-link text-center" id="sidenavToggler">
								<i class="fa fa-fw fa-angle-left"></i>
							</a>
						</li>
					</ul>
				</div>
			</nav>
			<div class="content-wrapper">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-6">

							<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
								Tambah Barang
							</button>

							<a href="<?php echo base_url();?>Traffic_data/menu_kategori"><button class="btn btn-success">Tambah kategori</button></a>

							<!-- form -->
							<form method="POST" action="<?php echo base_url();?>Traffic_data/input_barang">

								<!-- Modal -->
								<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Tambah Barang</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<table cellpadding="3px">
													<tr>
														<td>KODE BARANG</td>
														<td>:</td>
														<td><input type="text" name="kode_barang" autocomplete="off"></td>
													</tr>
													<tr>
														<td>KATEGORI</td>
														<td>:</td>
														<td>
															<select name="kategori">
																<option value="">--Pilih Kategori--</option>
																<?php foreach ($daftar_kategori->result() as $row) {?>
																<option><?php echo $row->nama_kategori; ?></option>
																<?php } ?>
															</select>
														</td>
													</tr>
													<tr>
														<td>MEREK</td>
														<td>:</td>
														<td><input type="text" name="merek"></td>
													</tr>
													<tr>
														<td>TIPE</td>
														<td>:</td>
														<td><input type="text" name="tipe" autocomplete="off"></td>
													</tr>
													<tr>
														<td>SIZE</td>
														<td>:</td>
														<td><input type="text" name="size" autocomplete="off"></td>
													</tr>
													<tr>
														<td>HARGA</td>
														<td>:</td>
														<td><input type="text" name="harga" autocomplete="off"></td>
													</tr>
												</table>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-primary">Simpan</button>
											</div>
										</div>
									</div>
								</div>
								<!-- modal -->
							</form>
							<!-- form -->

						</div>
					</div>

					<br>

					<div id="notifications-delete"><?php $this->session->flashdata('delete-message'); ?></div>
					<script type="text/javascript">
						$('#notifications-delete').slideDown('slow').delay(3200).slideUp('slow');
					</script>

					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table id="menu" class="table table-hover">
									<thead>
										<tr>
											<th>No</th>
											<th>Kode Barang</th>
											<th>Kategori</th>
											<th>Merek</th>
											<th>Tipe</th>
											<th>Size</th>
											<th>Harga</th>
											<th>Opsi</th>

										</tr>
										<tbody>
											<?php 
											$no = 0;
											foreach ($daftar_barang->result() as $row) {
												$no++;
												?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $row->kode_barang;?></td>
													<td><?php echo $row->kategori;?></td>
													<td><?php echo $row->merek;?></td>
													<td><?php echo $row->tipe;?></td>
													<td><?php echo $row->size;?></td>
													<td><?php echo 'Rp. '.number_format($row->harga).',-';?></td>

													<td>
														<button class="btn btn-warning" onclick="sendRequest('<?php echo base_url()?>Traffic_data/update_barang/<?php echo $row->kode_barang;?>', 'tampil_edit')">EDIT</button> 
														|
														<a href="<?php echo base_url();?>Traffic_data/hapus_barang/<?php echo $row->kode_barang;?>" onclick="return confirm('Apakah Anda yakin untuk menghapus kode barang : <?php echo $row->kode_barang; ?> ?' )"><button class="btn btn-danger">HAPUS</button></a>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</thead>
									</table>



									<script type="text/javascript">



								//javascript Document
								function getXMLHttpRequest(){
									return new XMLHttpRequest();
								}

								var xmlhttp=getXMLHttpRequest();

								function sendRequest(pageURL,ElementID){
									var obj=document.getElementById(ElementID);
									obj.innerHTML='Loading.....';
									xmlhttp.open('GET',pageURL,true);

									xmlhttp.onload = function(){
										obj.innerHTML=xmlhttp.responseText;
									}
									xmlhttp.send();
									setTimeout("sendRequest()",1000);
								}



								$(document).ready(function(){
									$('#menu').DataTable();
								});
							</script>
						</div>

						<div id="tampil_edit">

						</div>

					</div>

				</div>	


			</div>
			<!-- /.container-fluid-->
			<!-- /.content-wrapper-->
			<footer class="sticky-footer">
				<div class="container">
					<div class="text-center">
						<small>Dingdong</small>
					</div>
				</div>
			</footer>
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded" href="#page-top">
				<i class="fa fa-angle-up"></i>
			</a>
			<!-- Logout Modal-->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>	

			<!-- Bootstrap core JavaScript-->
			<!-- <script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery/jquery.min.js"></script> -->
			<script src="<?php echo base_url()?>assets/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
			<!-- Core plugin JavaScript-->
			<script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>
			<!-- Custom scripts for all pages-->
			<script src="<?php echo base_url()?>assets/sb-admin/js/sb-admin.min.js"></script>
			<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
		</div>


	</body>
	</html>