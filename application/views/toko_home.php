<!DOCTYPE html>
<html>
<head>
	<title>TOKO | Dashboard</title>
	<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/magopi-logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
	<link href="<?php echo base_url()?>assets/sb-admin/css/sb-admin.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<link href="<?php echo base_url()?>assets/sb-admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/jquery/jquery-ui.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery/jquery-3.3.1.min.js"></script>
	<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->

	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.dataTables.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/dataTables.bootstrap.css">

</head>
<body>


	<body class="fixed-nav sticky-footer bg-dark" id="page-top">
		<!-- Navigation-->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
			<a class="navbar-brand" href="">Dingdong TOKO</a>
			<a class="navbar-brand" href=""><?php echo $this->session->userdata("nama");?></a>
			<a class="navbar-brand" href=""></a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
					<li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="">
							<i class="fa fa-fw fa fa-desktop"></i>
							<span class="nav-link-text">Home</span>
						</a>
					</li>
					<!-- <!-- <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/buku_barang">
							<i class="fa fa-fw fa fa-book"></i>
							<span class="nav-link-text">Buku Barang</span>
						</a>
					-->
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Login/logout">
							<i class="fa fa-fw 	fa fa-user-circle-o"></i>
							<span class="nav-link-text">Logout</span>
						</a>
					</li>

				</ul>
				<ul class="navbar-nav sidenav-toggler">
					<li class="nav-item">
						<a class="nav-link text-center" id="sidenavToggler">
							<i class="fa fa-fw fa-angle-left"></i>
						</a>
					</li>
				</ul>
			</div>
		</nav>
		<div class="content-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-8">
							<div class="form-group">
								<label for="sel1">Masukan kunci pencarian:</label>
								<input type="text" name="kode_barang" id="kode_barang" class="form-control" onkeyup="sendRequest('tampil_barang');">
								<span id="peringatan" class="text-danger"></span>
							</div>

						</div>
					</div>
					<!-- col -->
				</div>
				<!-- row -->

				<br>

				<!-- row -->
				<div class="row">
					<div class= "col-md-12">
						<div class="table-responsive">
							<table class="table table-hover" id="">
								<thead>
									<tr>
										<th>Kategori</th>
										<th>Kode Barang</th>
										<th>Merek</th>
										<th>Tipe</th>
										<th>Harga Modal</th>
										<th>Lokasi Barang</th>
										<th>stok</th>
									</tr>
								</thead>
								<tbody id="tampil_barang">
									
								</tbody>	
							</table>
						</div>
					</div>
					<script type="text/javascript">
						$(document).ready(function(){
							$('#table').DataTable();
						});
					</script>
				</div>
				<!-- row -->

			</div>
			<!-- /.container-fluid-->
		</div>
		<!-- /.content-wrapper-->
		<footer class="sticky-footer">
			<div class="container">
				<div class="text-center">
					<small>Dingdong</small>
				</div>
			</div>
		</footer>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top">
			<i class="fa fa-angle-up"></i>
		</a>
		<!-- Logout Modal-->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<a class="btn btn-primary" href="login.html">Logout</a>
					</div>
				</div>
			</div>
		</div>	

		<!-- Bootstrap core JavaScript-->
		<!-- <script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery/jquery.min.js"></script> -->
		<script src="<?php echo base_url()?>assets/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<!-- Core plugin JavaScript-->
		<script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>
		<!-- Custom scripts for all pages-->
		<script src="<?php echo base_url()?>assets/sb-admin/js/sb-admin.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
	</div>

	<script type="text/javascript">
		//javascript Document
		function getXMLHttpRequest(){
			return new XMLHttpRequest();
		}

		var xmlhttp=getXMLHttpRequest();

		function sendRequest(ElementID){
			var obj=document.getElementById(ElementID);
			var kode_barang = document.getElementById("kode_barang").value;
			obj.innerHTML='Loading.....';
			xmlhttp.open('GET','<?php echo base_url()?>Traffic_data/cari_barang/'+kode_barang,true);

			xmlhttp.onload = function(){
				obj.innerHTML=xmlhttp.responseText;
			}
			xmlhttp.send();
			setTimeout("sendRequest()",1000);
		}

		$(document).ready(function(){
			$("#kode_barang").keyup(function(){
				if($(this).val()=='')
				{
					$(this).css('border-color', '#FF0000');
					$("#peringatan").text("Anda belum memasukan kata kunci !");
				}
				else
				{
					$(this).css("border-color", "#2eb82e");
					$("#peringatan").text("");
				}
			});
			
		});
	</script>


</body>
</html>