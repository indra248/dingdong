<!DOCTYPE html>
<html>
<head>
	<title>Admin | Dashboard</title>
	<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/magopi-logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
	<link href="<?php echo base_url()?>assets/sb-admin/css/sb-admin.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<link href="<?php echo base_url()?>assets/sb-admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/jquery/jquery-ui.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery/jquery-3.3.1.min.js"></script>
	<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->

	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.dataTables.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/dataTables.bootstrap.css">

</head>
<body>


	<body class="fixed-nav sticky-footer bg-dark" id="page-top">
		<!-- Navigation-->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
			<a class="navbar-brand" href="">ADMIN </a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/mainDashboard">
							<i class="fa fa-fw fa fa-desktop"></i>
							<span class="nav-link-text">Home</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/buku_barang">
							<i class="fa fa-fw fa fa-book"></i>
							<span class="nav-link-text">Buku Barang</span>
						</a>
					</li>
					<li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_stok">
							<i class="fa fa-fw fa fa-line-chart"></i>
							<span class="nav-link-text">Stok</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Daftar Menu">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_mutasi">
							<i class="fa fa-fw fa fa fa-exchange"></i>
							<span class="nav-link-text">Mutasi</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_barang_masuk">
							<i class="fa fa-fw 	fa fa-toggle-right"></i>
							<span class="nav-link-text">Barang Masuk</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_barang_keluar">
							<i class="fa fa-fw fa fa-toggle-left"></i>
							<span class="nav-link-text">Barang Keluar</span>
						</a>
					</li>

					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Login/logout">
							<i class="fa fa-fw 	fa fa-user-circle-o"></i>
							<span class="nav-link-text">Logout</span>
						</a>
					</li>
				</ul>
				<ul class="navbar-nav sidenav-toggler">
					<li class="nav-item">
						<a class="nav-link text-center" id="sidenavToggler">
							<i class="fa fa-fw fa-angle-left"></i>
						</a>
					</li>
				</ul>
			</div>
		</nav>
		<div class="content-wrapper">
			<div class="container-fluid">

				<!-- row -->
				<div class="row">
					<div class="col-md-2">
						<a href="<?php echo base_url();?>Traffic_data/daftarkan_barang"><button class="btn btn-success" >Daftarkan Barang</button></a>
					</div>

					<div class="col-md-2">
					<button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Download Stok</button>
					</div>
				</div>
				<!-- row -->






				<br>

				<nav>
					<div class="nav nav-tabs" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-gudang-1-tab" data-toggle="tab" href="#nav-gudang-1" role="tab" aria-controls="nav-gudang" aria-selected="true">GUDANG 1</a>


						<a class="nav-item nav-link" id="nav-gudang-2-tab" data-toggle="tab" href="#nav-gudang-2" role="tab" aria-controls="nav-gudang-2" aria-selected="false" onclick="sendRequest_gd2('gudang2')">GUDANG 2</a>


						<a class="nav-item nav-link" id="nav-toko-mami-tab" data-toggle="tab" href="#nav-toko-mami" role="tab" aria-controls="nav-toko-mami" aria-selected="false" onclick="sendRequest_tkmm('tkmm')">TOKO MAMI</a>

						<a class="nav-item nav-link" id="nav-toko-papi-tab" data-toggle="tab" href="#nav-toko-papi" role="tab" aria-controls="nav-contact" aria-selected="false"onclick="sendRequest_tkpp('tkpp')">TOKO PAPI</a>
					</div>
				</nav>


				<div class="tab-content" id="nav-tabContent">

					<!-- gudang-1 -->
					<div class="tab-pane fade show active" id="nav-gudang-1" role="tabpanel" aria-labelledby="nav-gudang-1-tab">
						<br>
						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-hover" id="gudang1">
										<thead>
											<tr>
												<th>No.</th>
												<th>Kode Barang</th>
												<th>Kategori</th>
												<th>Merek</th>
												<th>Tipe</th>
												<th>Size</th>
												<th>Jumlah Stok</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=0; foreach ($gudang1->result() as $row) { $no++;?>
											<tr>
												<td><?php echo $no; ?></td>
												<td><?php echo $row->kode_barang; ?></td>
												<td><?php echo $row->kategori;?></td>
												<td><?php echo $row->merek; ?></td>
												<td><?php echo $row->tipe; ?></td>
												<td><?php echo $row->size; ?></td>
												<td><?php echo $row->stok_barang; ?></td>
											</tr>
											<?php } ?>
										</tbody>
										
									</table>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#gudang1').DataTable();
										});
									</script>
								</div>
							</div>
						</div>
						<!-- row -->

					</div>
					<!-- gudang-1 -->

					<br>

					<!-- gudang 2 -->
					<div class="tab-pane fade" id="nav-gudang-2" role="tabpanel" aria-labelledby="nav-gudang-2-tab">

						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-hover" id="gd2">
										<thead>
											<tr>
												<th>No.</th>
												<th>Kode Barang</th>
												<th>Kategori</th>
												<th>Merek</th>
												<th>Tipe</th>
												<th>Size</th>
												<th>Jumlah Stok</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=0; foreach ($gudang2->result() as $row) { $no++;?>
											<tr>
												<td><?php echo $no; ?></td>
												<td><?php echo $row->kode_barang; ?></td>
												<td><?php echo $row->kategori;?></td>
												<td><?php echo $row->merek; ?></td>
												<td><?php echo $row->tipe; ?></td>
												<td><?php echo $row->size; ?></td>
												<td><?php echo $row->stok_barang; ?></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
									<script type="text/javascript">
										$(document).ready(function(){
										 $('#gd2').DataTable();
										});
									</script>
								</div>




							</div>
						</div>
						<!-- row -->

					</div>
					<!-- gudang 2 -->


					<!-- toko-mami -->
					<div class="tab-pane fade" id="nav-toko-mami" role="tabpanel" aria-labelledby="nav-toko-mami-tab">
						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-hover" id="toko-mami">
										<thead>
											<tr>
												<th>No.</th>
												<th>Kode Barang</th>
												<th>Kategori</th>
												<th>Merek</th>
												<th>Tipe</th>
												<th>Size</th>
												<th>Jumlah Stok</th>

											</tr>
										</thead>
										<tbody>
											<?php $no=0; foreach ($tkmm->result() as $row) { $no++;?>
											<tr>
												<td><?php echo $no; ?></td>
												<td><?php echo $row->kode_barang; ?></td>
												<td><?php echo $row->kategori;?></td>
												<td><?php echo $row->merek; ?></td>
												<td><?php echo $row->tipe; ?></td>
												<td><?php echo $row->size; ?></td>
												<td><?php echo $row->stok_barang; ?></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#toko-mami').DataTable();
										});
									</script>
								</div>
							</div>
						</div>
						<!-- row -->
					</div>
					<!-- toko-mami -->

					<!-- toko papi -->
					<div class="tab-pane fade" id="nav-toko-papi" role="tabpanel" aria-labelledby="nav-toko-papi-tab">

						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-hover" id="toko-papi">
										<thead>
											<tr>
												<th>No.</th>
												<th>Kode Barang</th>
												<th>Kategori</th>
												<th>Merek</th>
												<th>Tipe</th>
												<th>Size</th>
												<th>Jumlah Stok</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=0; foreach ($tkpp->result() as $row) { $no++;?>
											<tr>
												<td><?php echo $no; ?></td>
												<td><?php echo $row->kode_barang; ?></td>
												<td><?php echo $row->kategori;?></td>
												<td><?php echo $row->merek; ?></td>
												<td><?php echo $row->tipe; ?></td>
												<td><?php echo $row->size; ?></td>
												<td><?php echo $row->stok_barang; ?></td>
											</tr>
											<?php } ?>
										</tbody>
										<script type="text/javascript">
											$(document).ready(function(){
												$('#toko-papi').DataTable();
											});
										</script>
									</table>
								</div>
							</div>
						</div>
						<!-- row -->


					</div>
					<!-- toko-papi -->
				</div>






			</div>
			<!-- /.container-fluid-->
		</div>
		<!-- /.content-wrapper-->
		<footer class="sticky-footer">
			<div class="container">
				<div class="text-center">
					<small>Dingdong</small>
				</div>
			</div>
		</footer>
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top">
			<i class="fa fa-angle-up"></i>
		</a>
		<!-- download stok barang Modal-->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ready to Download?</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>

					<div class="modal-body">
						
						<form method="POST" action="<?php echo $act; ?>">

							<select class="form-control" name="kategori">
								<option>--Pilih Kategori--</option>
								<option value="all">Semua</option>								  
								<?php foreach ($kategori->result_array() as $key) {
								 echo "<option value=".$key["nama_kategori"].">".$key['nama_kategori']."</option>";} ?>
								}
							</select>

						

					</div>
					<!-- modal-body -->
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-danger">Proses</button>
						</form>
					</div>
				</div>
			</div>
		</div>	

		<!-- Bootstrap core JavaScript-->
		<!-- <script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery/jquery.min.js"></script> -->
		<script src="<?php echo base_url()?>assets/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<!-- Core plugin JavaScript-->
		<script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>
		<!-- Custom scripts for all pages-->
		<script src="<?php echo base_url()?>assets/sb-admin/js/sb-admin.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
	</div>


	<script type="text/javascript">
		//javascript Document
		// function getXMLHttpRequest(){
		// 	return new XMLHttpRequest();
		// }

		// var xmlhttp=getXMLHttpRequest();

		// function sendRequest_gd2(ElementID){
		// 	var obj=document.getElementById(ElementID);
		// 	var gudang2 = "GD-2";
		// 	obj.innerHTML='Loading.....';
		// 	xmlhttp.open('GET','<?php echo base_url()?>Traffic_data/gudang_cari/'+gudang2,true);

		// 	xmlhttp.onload = function(){
		// 		obj.innerHTML=xmlhttp.responseText;
		// 	}
		// 	xmlhttp.send();
		// 	setTimeout("sendRequest()",1000);
		// }

		// function sendRequest_tkmm(ElementID){
		// 	var obj=document.getElementById(ElementID);
		// 	var tkmm = "TK-MM";
		// 	obj.innerHTML='Loading.....';
		// 	xmlhttp.open('GET','<?php echo base_url()?>Traffic_data/gudang_cari/'+tkmm,true);

		// 	xmlhttp.onload = function(){
		// 		obj.innerHTML=xmlhttp.responseText;
		// 	}
		// 	xmlhttp.send();
		// 	setTimeout("sendRequest()",1000);
		// }

		// function sendRequest_tkpp(ElementID){
		// 	var obj=document.getElementById(ElementID);
		// 	var tkpp = "TK-PP";
		// 	obj.innerHTML='Loading.....';
		// 	xmlhttp.open('GET','<?php echo base_url()?>Traffic_data/gudang_cari/'+tkpp,true);

		// 	xmlhttp.onload = function(){
		// 		obj.innerHTML=xmlhttp.responseText;
		// 	}
		// 	xmlhttp.send();
		// 	setTimeout("sendRequest()",1000);
		// }
	</script>


</body>
</html>