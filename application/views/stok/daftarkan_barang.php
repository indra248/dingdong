<!DOCTYPE html>
<html>
<head>
	<title>Admin | Dashboard</title>
	<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/magopi-logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
	<link href="<?php echo base_url()?>assets/sb-admin/css/sb-admin.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<link href="<?php echo base_url()?>assets/sb-admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/jquery/jquery-ui.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery/jquery-3.3.1.min.js"></script>
	<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->

	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.dataTables.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/dataTables.bootstrap.css">

</head>
<body>


	<body class="fixed-nav sticky-footer bg-dark" id="page-top">
		<!-- Navigation-->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
			<a class="navbar-brand" href="">ADMIN </a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/mainDashboard">
							<i class="fa fa-fw fa fa-desktop"></i>
							<span class="nav-link-text">Home</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/buku_barang">
							<i class="fa fa-fw fa fa-book"></i>
							<span class="nav-link-text">Buku Barang</span>
						</a>
					</li>
					<li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_stok">
							<i class="fa fa-fw fa fa-line-chart"></i>
							<span class="nav-link-text">Stok  > Daftarkan Barang</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Daftar Menu">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_mutasi">
							<i class="fa fa-fw fa fa fa-exchange"></i>
							<span class="nav-link-text">Mutasi</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_barang_masuk">
							<i class="fa fa-fw 	fa fa-toggle-right"></i>
							<span class="nav-link-text">Barang Masuk</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="tables.html">
							<i class="fa fa-fw fa fa-toggle-left"></i>
							<span class="nav-link-text">Barang Keluar</span>
						</a>
					</li>

						<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Login/logout">
							<i class="fa fa-fw 	fa fa-user-circle-o"></i>
							<span class="nav-link-text">Logout</span>
						</a>
					</li>
				</ul>
				<ul class="navbar-nav sidenav-toggler">
					<li class="nav-item">
						<a class="nav-link text-center" id="sidenavToggler">
							<i class="fa fa-fw fa-angle-left"></i>
						</a>
					</li>
				</ul>
			</div>
		</nav>
		<div class="content-wrapper">
			<div class="container-fluid">


				<form method="POST" action="<?php echo base_url();?>Traffic_data/daftarkan_barang_proses">

				<!-- row -->
				<div class="row"> 
					<div class="col-md-6">	
						<table cellpadding="2px">

							<script type="text/javascript">


								function search()
								{
									$.ajax({
										type:"POST",
										url:"<?php echo base_url()?>Traffic_data/cek_kode_barang",
										data:{kode_barang : $('#kode_barang').val()},
										dataType:"json",
										beforeSend:function(e){
											if(e && e.overrideMimeType){
												e.overrideMimeType("application/json;charset=UTF-8");
											}
										},
										success:function(response){

											$('[name="merek"]').val(response.merek);
											$('[name="tipe"]').val(response.tipe);

										},
															  error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
															  	alert('Maaf Kode Barang Salah atau tidak terdaftar. Mohon cek manual ke menu Buku Barang. Terima kasih');
															  }
															});
								}
								$(document).ready(function(){
									$('#button_cari').click(function(){
										search();

									});
								});
							</script>
							<tr>
								<td>KODE BARANG</td>
								<td>:</td>
								<td><input type="text" id="kode_barang" required="" name="kode_barang" onkeyup="ambil();"></td>
								<td><button id="button_cari" type="button">CEK KODE</button></td>
							</tr>
							<tr>
								<td>Merek</td>
								<td></td>
								<td><input type="text" name="merek" required="" id="merek" readonly style="background: #E5E5E5;"></td>
							</tr>
							<tr>
								<td>Tipe</td>
								<td></td>
								<td><input type="text" name="tipe" id="tipe" required="" readonly="" style="background: #E5E5E5;"	></td>
							</tr>
						</table>
						<br>

				
						<p><b>Terdaftar di gudang :</b> <button type="button"  onclick="sendRequest('tampil_lokasi')">Cek Gudang</button></p>
						<table class="table">
							<thead>
							<tr>
								<th>Nama Lokasi</th>
								<th>Status Regist</th>
							</tr>	
							</thead>		
							<tbody id="tampil_lokasi">
								
							</tbody>					
						</table>

						<script type="text/javascript">
							
									//javascript Document
								function getXMLHttpRequest(){
									return new XMLHttpRequest();
								}

								var xmlhttp=getXMLHttpRequest();
								
								function sendRequest(ElementID){
									var kode_barang = document.getElementById('kode_barang').value;
									var obj=document.getElementById(ElementID);
									obj.innerHTML='Loading.....';
									xmlhttp.open('GET', "<?php echo base_url()?>Traffic_data/cek_lokasi_barang/"+kode_barang,true);

									xmlhttp.onload = function(){
										obj.innerHTML=xmlhttp.responseText;
									}
									xmlhttp.send(null);
									setTimeout("sendRequest()",1000);
								}
						</script>
						<div id="tampil_edit"></div>

					</div>

					<div class="col-md-6">	
						<table cellpadding="3px">
							<tr>
								<td>Daftar Lokasi Penyimpanan</td>
								<td>:</td>
								<td>
										<?php foreach ($lokasi_penyimpanan as $row) {?>
										<input type="checkbox" checked name="id_lokasi[]" value="<?php echo $row->id_lokasi;?>"><?php echo $row->id_lokasi;?>
										<br>
										<?php } ?>
								</td>
							</tr>


							<tr>
								<td>
									<button type="submit" class="btn btn-warning"><b>Daftarkan</b></button>
								</td>
							</tr>
						</table>

					</div>
					<div>
						<!-- row -->
						</form>

					</div>
					<!-- /.container-fluid-->
				</div>
				<!-- /.content-wrapper-->
				<footer class="sticky-footer">
					<div class="container">
						<div class="text-center">
							<small>Dingdong</small>
						</div>
					</div>
				</footer>
				<!-- Scroll to Top Button-->
				<a class="scroll-to-top rounded" href="#page-top">
					<i class="fa fa-angle-up"></i>
				</a>
				<!-- Logout Modal-->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<a class="btn btn-primary" href="login.html">Logout</a>
							</div>
						</div>
					</div>
				</div>	

				<!-- Bootstrap core JavaScript-->
				<!-- <script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery/jquery.min.js"></script> -->
				<script src="<?php echo base_url()?>assets/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
				<!-- Core plugin JavaScript-->
				<script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>
				<!-- Custom scripts for all pages-->
				<script src="<?php echo base_url()?>assets/sb-admin/js/sb-admin.min.js"></script>
				<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
			</div>


		</body>
		</html>