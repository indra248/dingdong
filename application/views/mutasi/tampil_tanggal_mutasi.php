
<!DOCTYPE html>
<html>
<head>  
  <title>Mutasi</title>  
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.css">
  <script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.js"></script>
</head>
<body>

 <div class="popup-wrapper" id="popup">
  <div class="popup-container">

    <fieldset>
      <div id="legend">
        <legend class="">MUTASI <a class="popup-close" href="">X</a></legend>

      </div>

      <div class="table-responsive">
        <?php
        $no = 0;
        foreach ($tampil_tanggal_mutasi->result() as $row) {$no++;?>
        <table cellpadding="15px">
          <tr>
            <th>No</th>
            <td>:&nbsp;</td>
            <td> <?php echo $no; ?></td>
          </tr>
          <tr>
            <th>Tanggal Mutasi</th>
            <td>:&nbsp;</td>
            <td> <?php echo $row->tanggal_mutasi; ?></td>
          </tr>
          <tr>
            <th>Petugas Admin</th>
            <td>:&nbsp;</td>
            <td><?php echo $row->petugas_admin; ?></td>
          </tr>
          <tr>
            <th>Petugas Lapangan</th>
            <td>:&nbsp;</td>
            <td><?php echo $row->petugas_lapangan; ?></td>
          </tr>
          <tr>
            <th>Keterangan</th>
            <td>:&nbsp;</td>
            <td>
              <?php
              if ($row->keterangan == "") {
                echo "-";
              }
              else
              {
                echo $row->keterangan;
              }  
              ?>

            </td>
          </tr>
        </table>
        <br>

        <table class="table table-hover" border="1">
          <thead>
            <th>Lokasi Pengambilan</th>
            <th>Lokasi Penyimpanan</th>
            <th>Kode Barang</th>
            <th>Banyak Barang</th>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $row->id_lokasi_pengambilan; ?></td>
              <td><?php echo $row->id_lokasi_penyimpanan; ?></td>
              <td><?php echo $row->kode_barang;?></td>
              <td><?php echo $row->banyak_barang;?></td>
            </tr>
          </tbody>
        </table>  

        <br><hr><br>
        <?php } ?>
      </fieldset>

    </div>

  </body>
  </html>