<!DOCTYPE html>
<html>
<head>
	<title>Admin | Dashboard</title>
	<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/magopi-logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
	<link href="<?php echo base_url()?>assets/sb-admin/css/sb-admin.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<link href="<?php echo base_url()?>assets/sb-admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/jquery/jquery-ui.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery/jquery-3.3.1.min.js"></script>
	<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->

	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.dataTables.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/DataTables/media/css/dataTables.bootstrap.css">

</head>
<body>


	<body class="fixed-nav sticky-footer bg-dark" id="page-top">
		<!-- Navigation-->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
			<a class="navbar-brand" href="">ADMIN </a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/mainDashboard">
							<i class="fa fa-fw fa fa-desktop"></i>
							<span class="nav-link-text">Home</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/buku_barang">
							<i class="fa fa-fw fa fa-book"></i>
							<span class="nav-link-text">Buku Barang</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kasir">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_stok">
							<i class="fa fa-fw fa fa-line-chart"></i>
							<span class="nav-link-text">Stok</span>
						</a>
					</li>
					<li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Daftar Menu">
						<a class="nav-link" href="<?php echo base_url()?>Traffic_data/menu_mutasi">
							<i class="fa fa-fw fa fa fa-exchange"></i>
							<span class="nav-link-text">Mutasi > Input Mutasi</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_barang_masuk">
							<i class="fa fa-fw 	fa fa-toggle-right"></i>
							<span class="nav-link-text">Barang Masuk</span>
						</a>
					</li>
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Traffic_data/menu_barang_keluar">
							<i class="fa fa-fw fa fa-toggle-left"></i>
							<span class="nav-link-text">Barang Keluar</span>
						</a>
					</li>
					
					<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Jurnal Transaksi">
						<a class="nav-link" href="<?php echo base_url();?>Login/logout">
							<i class="fa fa-fw 	fa fa-user-circle-o"></i>
							<span class="nav-link-text">Logout</span>
						</a>
					</li>
				</ul>
				<ul class="navbar-nav sidenav-toggler">
					<li class="nav-item">
						<a class="nav-link text-center" id="sidenavToggler">
							<i class="fa fa-fw fa-angle-left"></i>
						</a>
					</li>
				</ul>
			</div>
		</nav>
		<div class="content-wrapper">
			<div class="container-fluid">


				<form method="POST" action="<?php echo base_url();?>Traffic_data/input_mutasi_proses">

					<!-- row -->
					<div class="row"> 
						<div class="col-md-6">	
							<table cellpadding="2px">

								<script type="text/javascript">
									function search()
									{
										$.ajax({
											type:"POST",
											url:"<?php echo base_url()?>Traffic_data/cek_kode_barang",
											data:{kode_barang : $('#kode_barang').val()},
											dataType:"json",
											beforeSend:function(e){
												if(e && e.overrideMimeType){
													e.overrideMimeType("application/json;charset=UTF-8");
												}
											},
											success:function(response){

												$('[name="merek"]').val(response.merek);
												$('[name="tipe"]').val(response.tipe);

											},
															  error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
															  	alert('Maaf Kode Barang Salah atau tidak terdaftar. Mohon cek manual ke menu Buku Barang. Terima kasih');
															  }
															});
									}
									$(document).ready(function(){
										$('#button_cari').click(function(){
											search();

										});
									});
								</script>
								<tr style="display: none;">
									<td>PETUGAS</td>
									<td>:</td>
									<td><input type="text" id="nama_admin" required="" readonly="" name="petugas_admin" value="<?php echo $this->session->userdata('nama');?>" style="background: #E5E5E5;"></td>
								</tr>
								<tr>
									<td>KODE BARANG</td>
									<td>:</td>
									<td><input type="text" id="kode_barang" required="" name="kode_barang"></td>
									<td><button  id="button_cari" type="button" onclick="sendRequest('lokasi_dan_stok');">CEK KODE</button></td>
								</tr>
								<tr>
									<td>Merek</td>
									<td></td>
									<td><input type="text" name="merek" required="" id="merek" readonly style="background: #E5E5E5;"></td>
								</tr>
								<tr>
									<td>Tipe</td>
									<td></td>
									<td><input type="text" name="tipe" id="tipe" required="" readonly="" style="background: #E5E5E5;"	></td>
								</tr>
							</table>
							<br>

							<table>

								<tr>
									<td><strong>Petugas Lapangan</strong></td>
									<td>:</td>
									<td>
										<select name="petugas_lapangan" class="form-control form-control-md" ="300px" required>
											<option value="">--Pilih Petugas--</option>
											<?php foreach ($petugas_lapangan->result() as $row) {?>
												<option value="<?php echo $row->nama;?>"><?php echo $row->nama;?></option>
											<?php } ?>
										</select>
									</td>
								</tr>
						</table>
						<br>
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Nama Lokasi</th>
										<th>Stok</th>
									</tr>
									<tbody id="lokasi_dan_stok">

									</tbody>
								</thead>
							</table>
						</div>



					</div>

					<div class="col-md-6">	
						<table cellpadding="3px">
							<tr>
								<td>Tanggal Mutasi barang</td>
								<td>:</td>
								<td><input type="date" required="" name="tanggal_mutasi"></td>
							</tr>
							<tr>
								<td>Banyak Barang</td>
								<td>:</td>
								<td><input type="text" width="10px" required="" size="2" name="banyak_barang"></td>
							</tr>
							<tr>
								<td>Pilih Lokasi Pengambilan</td>
								<td>:</td>
								<td>
									<select name="id_lokasi_pengambilan" required="">
										<option>--Pilih Lokasi</option>
										<?php foreach ($lokasi_penyimpanan as $row) {?>
											<option value="<?php echo $row->id_lokasi;?>"><?php echo $row->nama_lokasi_penyimpanan ?></option>
										<?php } ?>

									</select>
								</td>
							</tr>
							<tr>
								<td>Pilih Lokasi Mutasi</td>
								<td>:</td>
								<td>
									<select name="id_lokasi_penyimpanan" required="">
										<option>--Pilih Lokasi</option>
										<?php foreach ($lokasi_penyimpanan as $row) {?>
											<option value="<?php echo $row->id_lokasi;?>"><?php echo $row->nama_lokasi_penyimpanan ?></option>
										<?php } ?>

									</select>
								</td>
							</tr>
							<tr>
								<td>Keterangan (Opsional)</td>
								<td>:</td>
								<td><textarea name="keterangan" placeholder=""></textarea></td>
							</tr>

							<tr>
								<td>
									<button type="submit" class="btn btn-warning">Simpan</button>
								</td>
							</tr>
						</table>

					</div>

					<div>
						<!-- row -->
					</form>




				</div>
				<!-- /.container-fluid-->
			</div>
			<!-- /.content-wrapper-->

			<br>

			<!-- row -->
			<div class="row">
				<div class="col-md-12">
					<h6>*<i>Data diambil berdasarkan bulan ini.</i></h6>
				</div>

			</div>
			<!-- row -->


			<div class="row">
				<div class="col-md-12">

					<div class="table-responsive">
						<table border="1" class="" id="">
							<thead>
								<tr>
									<th>No.</th>
									<th>Tanggal Mutasi</th>
									<th>Petugas Admin</th>
									<th>Petugas Lapangan</th>
									<th>Lokasi Pengambilan</th>
									<th>Lokasi Penyimpanan</th>
									<th>Kode Barang</th>
									<th>Banyak Barang</th>
									<th>Keterangan</th>
								</tr>
							</thead>
							<tbody align="center">
								<?php $no=0; foreach ($daftar_mutasi->result() as $row ){$no++;?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $row->tanggal_mutasi; ?></td>
										<td><?php echo $row->petugas_admin; ?></td>
										<td><?php echo $row->petugas_lapangan; ?></td>
										<td><?php echo $row->id_lokasi_pengambilan; ?></td>
										<td><?php echo $row->id_lokasi_penyimpanan; ?></td>
										<td><?php echo $row->kode_barang; ?></td>
										<td><?php echo $row->banyak_barang; ?></td>
										<td><?php echo $row->keterangan; ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#data_barang_keluar').DataTable();
							});
						</script>
					</div>

				</div>

			</div>




			<footer class="sticky-footer">
				<div class="container">
					<div class="text-center">
						<small>Dingdong</small>
					</div>
				</div>
			</footer>
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded" href="#page-top">
				<i class="fa fa-angle-up"></i>
			</a>
			<!-- Logout Modal-->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>	

			<!-- Bootstrap core JavaScript-->
			<!-- <script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery/jquery.min.js"></script> -->
			<script src="<?php echo base_url()?>assets/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
			<!-- Core plugin JavaScript-->
			<script src="<?php echo base_url()?>assets/sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>
			<!-- Custom scripts for all pages-->
			<script src="<?php echo base_url()?>assets/sb-admin/js/sb-admin.min.js"></script>
			<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
		</div>

		<script type="text/javascript">

					//javascript Document
					function getXMLHttpRequest(){
						return new XMLHttpRequest();
					}

					var xmlhttp=getXMLHttpRequest();

					function sendRequest(ElementID){
						var kode_barang = document.getElementById('kode_barang').value;
						var obj=document.getElementById(ElementID);
						obj.innerHTML='Loading.....';
						xmlhttp.open('GET','<?php echo base_url();?>/Traffic_data/lokasi_dan_stok/'+kode_barang,true);

						xmlhttp.onload = function(){
							obj.innerHTML=xmlhttp.responseText;
						}
						xmlhttp.send();
						setTimeout("sendRequest()",1000);
					}

				</script>


			</body>
			</html>