
<!DOCTYPE html>
<html>
<head>  
  <title>Mutasi</title>  
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.css">
  <script type="text/javascript" src="<?php echo base_url()?>assets/DataTables/media/js/jquery.js"></script>
</head>
<body>

 <div class="popup-wrapper" id="popup">
  <div class="popup-container">

    <fieldset>
      <div id="legend">
        <legend class="">MUTASI <a class="popup-close" href="">X</a></legend>

      </div>

      <div class="table-responsive">
        <?php
        $no = 0;
        foreach ($tampil_tanggal_masuk->result() as $row) {$no++;?>
        <table cellpadding="15px" border="1">
          <tr>
            <th>NO</th>
            <td>:&nbsp;</td>
            <td> <?php echo $no; ?></td>
          </tr>
          <tr>
            <th>TANGGAL MUTASI</th>
            <td>:&nbsp;</td>
            <td> <?php echo $row->tanggal_masuk_barang; ?></td>
          </tr>
          <tr>
            <th>PETUGAS ADMIN</th>
            <td>:&nbsp;</td>
            <td><?php echo $row->nama; ?></td>
          </tr>
           <tr>
            <th>KETERANGAN</th>
            <td>:&nbsp;</td>
            <td><?php echo $row->keterangan; ?></td>
          </tr>
        </table>
        <br>

        <table class="table table-hover" border="2">
          <thead>
            <th>LOKASI PENYIMPANAN</th>
            <th>KODE BARANG</th>
            <th>BANYAK BARANG</th>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $row->nama_lokasi_penyimpanan; ?></td>
              <td><?php echo $row->kode_barang;?></td>
              <td><?php echo $row->banyak_barang;?></td>
            </tr>
          </tbody>
        </table>  

        <br><hr><br>
        <?php } ?>
      </fieldset>

    </div>

  </body>
  </html>