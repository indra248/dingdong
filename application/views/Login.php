<!DOCTYPE html>
<html>
<head>
	<title>Dingdong Warehouse</title>
	<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/magopi-logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery/jquery-3.3.1.min.js"></script>
</head>
<body style="background:#009688;">

	<div class="container py-5">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center text- mb-4" style="color:#F5F2F2;"><b>Dingdong Elektronik</b> ||  Warehouse</h2>
				<div class="row">
					<div class="col-md-6 mx-auto">

						<!-- form card login -->
						<form method="POST" action="<?php echo base_url()?>login/validation">
							<div class="card rounded-0" style="background:#E8EAF6;">
								<div class="card-header">
									<h4 class="mb-0"><b style="color:#595757;">Login</b></h4>
								</div>
								<div class="card-body">
									<form class="form" role="form" autocomplete="off" id="formLogin" novalidate="" method="POST">
										<div class="form-group">
											<label>Nama Admin</label>
											<input type="text" class="form-control form-control-lg rounded-0" id="pwd1" required="" autocomplete="new-password" name="nama_admin">
											<div class="invalid-feedback">Enter your password too!</div>
										</div>
										<div class="form-group">
											<label>Password</label>
											<input type="password" class="form-control form-control-lg rounded-0" id="pwd1" required="" autocomplete="new-password" name="password">
											<div class="invalid-feedback">Enter your password too!</div>
										</div>

										<button type="submit" class="btn btn-success btn-lg float-right" id="btnLogin">Login</button>
									</form>
								</div>
								<!--/card-block-->
							</div>
						</form>
						<!-- /form card login -->

					</div>


				</div>
				<!--/row-->

			</div>
			<!--/col-->
		</div>
		<!--/row-->
	</div>
	<!--/container-->

</body>
<script type="text/javascript">

	$("#btnLogin").click(function(event) {

    //Fetch form to apply custom Bootstrap validation
    var form = $("#formLogin")

    if (form[0].checkValidity() === false) {
    	event.preventDefault()
    	event.stopPropagation()
    }
    
    form.addClass('was-validated');
});
</script>
</html>