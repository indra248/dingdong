<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("login_check");

	}


	public function index()
	{
		$this->load->view("login.php");
	}


	public function validation()
	{

		$nama_admin = $this->input->POST("nama_admin");
		$password = $this->input->POST("password");
		$this->login_check->validation($nama_admin, $password);

	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect("Login");
	}

	public function deny_access()
	{
		$this->load->view("deny_access.php");
	}

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */