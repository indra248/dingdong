  <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Traffic_data extends CI_Controller {

  	public function __construct()
  	{
  		parent::__construct();

  		$this->load->model("Security");
  		$this->Security->security_check();

  		$this->load->model("Db_connect");

  	}


  	public function mainDashboard()
  	{
  		if ($this->session->userdata('level_user') == '2' or $this->session->userdata('level_user') == '1') 
  		{
  			$this->load->view("admin_home.php");	
  		}
  		elseif($this->session->userdata('level_user') == '3')
  		{
  			$this->load->view("toko_home.php");
  		}
  	}

    function cari_barang($kode_barang = "Dummy For Hold Query")
    {
      try 
      {

       $result['tampil_barang'] = $this->Db_connect->cari_barang($kode_barang);
       $this->load->view("cari_barang.php", $result);
     } 

  		//menangkap value buangan yang error.
     catch (ArgumentCountError $e) {
       echo $e->getMessage();
     }catch (Exception $er) {
       echo $er->getMessage();
     }
  		//menangkap value buangan yang error.
   }



	// buku barang
   public function buku_barang()
   {
    $result['daftar_kategori'] = $this->Db_connect->daftar_kategori();
    $result['daftar_barang'] = $this->Db_connect->daftar_barang();
    $this->load->view("barang/buku_barang.php", $result);
  }

  public function input_barang()
  {
    $data = array(
     'kode_barang' => $this->input->POST('kode_barang'),
     'kategori' => $this->input->POST('kategori'),
     'merek' => $this->input->POST('merek'),
     'tipe' => $this->input->POST('tipe'),
     'size' => $this->input->POST('size'),
     'harga' => $this->input->POST('harga')
   );

    $data_lokasi_barang = array(
     'id_lokasi' => $this->input->POST('id_lokasi'),	
     'kode_barang' => $this->input->POST('kode_barang'),
     'stok_barang' => $this->input->POST('stok_barang')	
   );

    $this->Db_connect->input_barang($data);
    redirect("Traffic_data/buku_barang");
  }

  public function hapus_barang($kode_barang)
  {
    $this->Db_connect->hapus_barang($kode_barang);
    $this->session->set_flashdata('delete-message', '<div class="alert alert-danger">
     <h4><i class="glyphicon glyphicon-trash"></i> Delete Success </h4>
     <p>You are Command has Succeed '.' Just Now </p>
     </div>');		
    redirect("Traffic_data/buku_barang");
  }

  public function update_barang($kode_barang)
  {
    $result['update_barang'] = $this->Db_connect->get_kode_barang($kode_barang);
    $this->load->view("barang/form_update_barang.php", $result);
  }

  public function update_barang_proses($kode_barang)
  {
    $data = array(
     'kategori' => $this->input->POST('kategori'),
     'merek' => $this->input->POST('merek'),
     'tipe' => $this->input->POST('tipe'),
     'size' => $this->input->POST('size'),
     'harga' => $this->input->POST('harga')
   );

    $this->Db_connect->update_barang_proses($data,$kode_barang);
    redirect("Traffic_data/buku_barang");
  }

  public function menu_kategori()
  {
    $result['daftar_kategori'] = $this->Db_connect->daftar_kategori();
    $this->load->view("barang/menu_kategori.php", $result);
  }

  public function tambah_kategori()
  {

    $data = array(
     'id_kategori' => '0',
     'nama_kategori' => $this->input->POST('nama_kategori')
   );

    $this->Db_connect->tambah_kategori($data);
  }

	// buku barang



	//menu stok
  public function menu_stok()
  {

    $result['gudang1'] = $this->Db_connect->gudang1();
    $result['gudang2'] = $this->Db_connect->gudang2();
    $result['tkmm']    = $this->Db_connect->tkmm();
    $result['tkpp']    = $this->Db_connect->tkpp();
    $result['act']     = site_url().'Traffic_data/downloadstok';
    $result['kategori']= $this->Db_connect->daftar_kategori();

    extract($result);



    $this->load->view("stok/menu_stok.php", $result);
  }

  public function daftarkan_barang()
  {
    $result['lokasi_penyimpanan'] = $this->Db_connect->lokasi_penyimpanan();
    $this->load->view("stok/daftarkan_barang.php", $result);
  }

  public function daftarkan_barang_proses()
  {

    $lokasi = $this->input->POST('id_lokasi');
    $count_id_lokasi = count($lokasi);
    for ($i=0; $i <$count_id_lokasi ; $i++) { 

      $data = array(
       'id_lokasi' => $lokasi[$i],
       'kode_barang' => $this->input->POST('kode_barang'),
       'stok_barang' => ""
     );
      $id_lokasi = $this->input->POST('id_lokasi');
      $kode_barang = $this->input->POST('kode_barang');
      $this->db->insert("lokasi_barang", $data);
    // $this->Db_connect->daftarkan_barang_proses($data, $id_lokasi, $kode_barang);
    }
    redirect("Traffic_data/daftarkan_barang");


  }

  public function cek_lokasi_barang($kode_barang = null)
  {
    if (empty($kode_barang)) {
     echo "ERROR";
   }
   else
   {
     $result['cek_lokasi_barang'] = $this->Db_connect->cek_lokasi_barang($kode_barang);
     $this->load->view("stok/tampil_lokasi_penyimpanan", $result);
   }	

 }

 public function gudang_cari($id_lokasi = null)
 {
  $result['gudang_cari'] = $this->Db_connect->gudang_cari($id_lokasi);
  $this->load->view("stok/tampil_lokasi.php", $result);
}


function downloadstok($kategori = '')
{
 $this->load->helper('download');
 $month = date('m');
  // $year =  $this->input->POST("tahun");
 $month_array = array( "--Select Month --","Januari", "Februari", "Maret", "April", "Mei", "Juni", "July", "Agustus", "September", "Oktober", "November", "Desember");
 $month_now = $month_array[$month];

 $year = date('Y');

 include 'assets/PHPExcel/PHPExcel.php';

    // Panggil class PHPExcel nya
 $excel = new PHPExcel();
    // Settingan awal fil excel
 $creator = $this->session->userdata('nama');
 $excel->getProperties()->setCreator($creator)
 ->setLastModifiedBy('Dingdong Admin')
 ->setTitle("Dingdong Stock")
 ->setSubject("Dingdong Inventory")
 ->setDescription("Laporan Stok")
 ->setKeywords("Stok Barang");

 // $objPHPExcel = new PHPExcel;
    $objSheet = $excel->getActiveSheet();
    $objSheet->getProtection()->setSheet(true);

    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
 $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
 $style_row = array(
  'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
  'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
);
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "LAPORAN STOK BARANG ".$month_now.'/'.$year); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:H1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(24); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "KATEGORI"); 
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "KODE BARANG");
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "MEREK");
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "TIPE");
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "SIZE");
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "HARGA MODAL");
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "JUMLAH STOK");


    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    

    $kategori = $this->input->POST('kategori');
    $downloadstok = $this->Db_connect->downloadstok($kategori);
    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach($downloadstok as $data){ // Lakukan looping pada variabel
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->kategori);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->kode_barang);
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->merek);
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->tipe);
      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->size);
      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, 'Rp. '.number_format($data->harga).',-');
      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->jumlah);

      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);

      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(17); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(16); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Mutasi Bulan ".$month_now);
    $excel->setActiveSheetIndex(0);
    // Proses file excel



    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    ob_end_clean();
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Stok Barang'.$month_now.' '.$year.'.xlsx"'); // Set nama file excel nya


    $write->save('php://output');

  }

	//menu stok


	//menu_barang_masuk

  public function menu_barang_masuk()
  {
    $result['tanggal_masuk_barang'] = $this->Db_connect->tanggal_masuk_barang();
    $this->load->view("barang_masuk/menu_barang_masuk.php", $result);
  }

  public function tampil_tanggal_masuk($tanggal_masuk)
  {
    $result['tampil_tanggal_masuk'] = $this->Db_connect->tampil_tanggal_masuk($tanggal_masuk);
    $this->load->view("barang_masuk/tampil_tanggal_masuk.php", $result);
  }

  public function input_barang_masuk()
  {
    $result['lokasi_penyimpanan'] = $this->Db_connect->lokasi_penyimpanan();
    $result['data_barang_masuk'] = $this->Db_connect->data_barang_masuk();

    $this->load->view("barang_masuk/input_barang_masuk.php", $result);
  }

  public function cek_kode_barang()
  {
    $kode_barang = $this->input->POST('kode_barang');
    $query = $this->Db_connect->cek_kode_barang($kode_barang);

    foreach ($query->result() as $row) 
     $rslt = array('kode_barang' => $row->kode_barang, 'merek' => $row->merek, 'tipe' => $row->tipe);
   echo json_encode($rslt);
 }

 public function input_barang_masuk_proses()
 {
  $data = array(
   'kbarang_masuk' => '0',	
   'kode_barang' => $this->input->POST('kode_barang'),
   'tanggal_masuk_barang' => $this->input->POST('tanggal_masuk_barang'),
   'banyak_barang' => $this->input->POST('banyak_barang'),
   'id_lokasi' => $this->input->POST('id_lokasi'),
   'id_karyawan' => $this->input->POST('nama_admin'),
   'keterangan' => $this->input->POST('keterangan')
 );
  $kode_barang = $this->input->POST('kode_barang');
  $id_lokasi   = $this->input->POST('id_lokasi');

  $this->Db_connect->input_barang_masuk_proses($data, $kode_barang, $id_lokasi);
}


	//menu_barang_masuk



	//menu barang keluar
public function menu_barang_keluar()
{
  $result['tanggal_keluar_barang'] = $this->Db_connect->tanggal_keluar_barang();
  $this->load->view("barang_keluar/menu_barang_keluar", $result);
}

public function input_barang_keluar()
{
  $result['lokasi_penyimpanan'] = $this->Db_connect->lokasi_penyimpanan();
  $result['petugas_lapangan'] = $this->Db_connect->petugas_lapangan();
  $result['data_barang_keluar'] = $this->Db_connect->data_barang_keluar();

  $this->load->view("barang_keluar/input_barang_keluar.php", $result);
}

public function tampil_tanggal_keluar($tanggal_keluar)
{
  $result['tampil_tanggal_keluar'] = $this->Db_connect->tampil_tanggal_keluar($tanggal_keluar);
  $this->load->view("barang_keluar/tampil_tanggal_keluar.php", $result);
}

public function input_barang_keluar_proses()
{
  $data = array(
   'kbarang_keluar' => '0',
   'petugas_admin' => $this->input->POST('nama_admin'),
   'petugas_lapangan' => $this->input->POST('petugas_lapangan'),
   'tanggal_keluar_barang' => $this->input->POST('tanggal_keluar_barang'),
   'kode_barang' => $this->input->POST('kode_barang'),
   'banyak_barang' => $this->input->POST('banyak_barang'),
   'id_lokasi' => $this->input->POST('id_lokasi'),
   'keterangan' => $this->input->POST('keterangan') 
 );

  $kode_barang = $this->input->POST('kode_barang');
  $id_lokasi   = $this->input->POST('id_lokasi');
  $this->Db_connect->input_barang_keluar_proses($data, $kode_barang, $id_lokasi);
}

	//menu barang keluar


	// menu mutasi

public function menu_mutasi()

{

  $result['data_mutasi_barang'] = $this->Db_connect->data_mutasi_barang();
  $this->load->view("mutasi/menu_mutasi.php", $result);
}

public function tampil_tanggal_mutasi($tanggal_mutasi)
{
  $result['tampil_tanggal_mutasi']= $this->Db_connect->tampil_tanggal_mutasi($tanggal_mutasi);	
  $this->load->view("mutasi/tampil_tanggal_mutasi.php", $result);
}

public function input_mutasi()
{
  $result['daftar_mutasi'] = $this->Db_connect->daftar_mutasi();
  $result['lokasi_penyimpanan'] = $this->Db_connect->lokasi_penyimpanan();
  $result['petugas_lapangan'] = $this->Db_connect->petugas_lapangan();
  $this->load->view("mutasi/input_mutasi.php", $result);
}

public function lokasi_dan_stok($kode_barang = '')
{
  $result['lokasi_dan_stok'] = $this->Db_connect->lokasi_dan_stok($kode_barang);
  $this->load->view("mutasi/lokasi_dan_stok.php", $result);
}

public function input_mutasi_proses()
{
  $data = array(
   'kmutasi_barang' => '0',
   'tanggal_mutasi' => $this->input->POST('tanggal_mutasi'),
   'petugas_admin' => $this->input->POST('petugas_admin'),
   'petugas_lapangan' => $this->input->POST('petugas_lapangan'),
   'id_lokasi_pengambilan' => $this->input->POST('id_lokasi_pengambilan'),
   'id_lokasi_penyimpanan' => $this->input->POST('id_lokasi_penyimpanan'),
   'kode_barang' => $this->input->POST('kode_barang'),
   'banyak_barang' => $this->input->POST('banyak_barang'),
   'keterangan' => $this->input->POST('keterangan')
 );

  $id_lokasi_pengambilan = $this->input->POST('id_lokasi_pengambilan');
  $id_lokasi_penyimpanan = $this->input->POST('id_lokasi_penyimpanan');
  $kode_barang 		   = $this->input->POST('kode_barang');

  $this->Db_connect->input_mutasi_proses($data, $id_lokasi_pengambilan, $id_lokasi_penyimpanan, $kode_barang);


}
	// menu mutasi


    // Download Mutation By Month


public function downloadxls()
{
  $this->load->helper('download');
  $month = $this->input->POST("bulan");
  $year =  $this->input->POST("tahun");
  $month_array = array( "--Select Month --","Januari", "Februari", "Maret", "April", "Mei", "Juni", "July", "Agustus", "September", "Oktober", "November", "Desember");
  $month_now = $month_array[$month];

  include 'assets/PHPExcel/PHPExcel.php';

    // Panggil class PHPExcel nya
  $excel = new PHPExcel();
    // Settingan awal fil excel
  $excel->getProperties()->setCreator('Indra Ryanto Pandean')
  ->setLastModifiedBy('Dingdong Admin')
  ->setTitle("Dingdong Freight Mutation")
  ->setSubject("Dingdong Inventory")
  ->setDescription("Laporan Mutasi")
  ->setKeywords("Mutasi Barang");



    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
  $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
  $style_row = array(
    'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
    'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
  );
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "LAPORAN MUTASI BARANG BULAN ".$month_now.'/'.$year); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:L1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(24); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "TANGGAL MUTASI"); 
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "ADMIN");
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "PETUGAS LAPANGAN");
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "PENGAMBILAN");
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "PENYIMPANAN");
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "KODE BARANG");
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "KATEGORI");
    $excel->setActiveSheetIndex(0)->setCellValue('I3', "MEREK");   
    $excel->setActiveSheetIndex(0)->setCellValue('J3', "TIPE");
    $excel->setActiveSheetIndex(0)->setCellValue('K3', "QTY");
    $excel->setActiveSheetIndex(0)->setCellValue('L3', "KETERANGAN");


    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
    
    $MutationByMonth = $this->Db_connect->download_mutation_ByMonth($month, $year);
    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach($MutationByMonth as $data){ // Lakukan looping pada variabel
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->tanggal_mutasi);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->petugas_admin);
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->petugas_lapangan);
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->id_lokasi_pengambilan);
      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->id_lokasi_penyimpanan);
      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->kode_barang);
      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->kategori);
      $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->merek);
      $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->tipe);
      $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $data->banyak_barang);
      $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $data->keterangan);

      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(17); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(16); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
    $excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Mutasi Bulan ".$month_now);
    $excel->setActiveSheetIndex(0);
    // Proses file excel

    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    ob_end_clean();
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Mutasi Bulan '.$month_now.' '.$year.'.xlsx"'); // Set nama file excel nya
    $write->save('php://output');
  }

    // Download Mutation By Month


  // Download barang keluar


  public function download_stok_outByMonth()
  {

    $this->load->helper('download');
    $month = $this->input->POST("bulan");
    $year  =  $this->input->POST("tahun");
    $month_array = array( "--Select Month --","Januari", "Februari", "Maret", "April", "Mei", "Juni", "July", "Agustus", "September", "Oktober", "November", "Desember");
    $month_now = $month_array[$month];

    include 'assets/PHPExcel/PHPExcel.php';

    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Indra Ryanto Pandean')
    ->setLastModifiedBy('Dingdong Admin')
    ->setTitle("Dingdong Freight Stock Out")
    ->setSubject("Dingdong Inventory")
    ->setDescription("Laporan Barang Keluar")
    ->setKeywords("Barang Keluar");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "LAPORAN BARANG KELUAR BULAN ".$month_now.'/'.$year); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:L1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(24); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "TANGGAL"); 
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "ADMIN");
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "PETUGAS LAPANGAN");
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "LOKASI");
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "KODE BARANG");
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "KATEGORI");
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "MEREK");   
    $excel->setActiveSheetIndex(0)->setCellValue('I3', "TIPE");
    $excel->setActiveSheetIndex(0)->setCellValue('J3', "QTY");
    $excel->setActiveSheetIndex(0)->setCellValue('K3', "KETERANGAN");


    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
    
    $StockOutByMonth = $this->Db_connect->download_stok_outByMonth($month, $year);
    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach($StockOutByMonth as $data){ // Lakukan looping pada variabel
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->tanggal_keluar_barang);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->petugas_admin);
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->petugas_lapangan);
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->nama_lokasi_penyimpanan);
      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->kode_barang);
      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->kategori);
      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->merek);
      $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->tipe);
      $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->banyak_barang);
      $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $data->keterangan);

      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(14); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(16);
    $excel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
    $excel->getActiveSheet()->getColumnDimension('K')->setWidth(40);

    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Barang Keluar Bulan ".$month_now);
    $excel->setActiveSheetIndex(0);
    // Proses file excel

    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    ob_end_clean();
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Barang keluar '.$month_now.' '.$year.'.xlsx"'); // Set nama file excel nya
    $write->save('php://output');
  }

  // Dowload barang keluar


  //Download barang masuk

  public function download_stok_inByMonth()
  {

    $this->load->helper('download');
    $month = $this->input->POST("bulan");
    $year  =  $this->input->POST("tahun");
    $month_array = array( "--Select Month --","Januari", "Februari", "Maret", "April", "Mei", "Juni", "July", "Agustus", "September", "Oktober", "November", "Desember");
    $month_now = $month_array[$month];

    include 'assets/PHPExcel/PHPExcel.php';

    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Indra Ryanto Pandean')
    ->setLastModifiedBy('Dingdong Admin')
    ->setTitle("Dingdong Freight Stock In")
    ->setSubject("Dingdong Inventory")
    ->setDescription("Laporan Barang Masuk")
    ->setKeywords("Barang Masuk");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "LAPORAN BARANG MASUK BULAN ".$month_now.'/'.$year); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:I1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(24); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "TANGGAL"); 
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "ADMIN");
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "KODE BARANG");
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "KATEGORI");
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "MEREK");   
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "TIPE");
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "QTY");
    $excel->setActiveSheetIndex(0)->setCellValue('I3', "KETERANGAN");


    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);

    $StockInByMonth = $this->Db_connect->download_stok_inByMonth($month, $year);
    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach($StockInByMonth as $data){ // Lakukan looping pada variabel
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->tanggal_masuk_barang);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->nama);
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->kode_barang);
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->kategori);
      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->merek);
      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->tipe);
      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->banyak_barang);
      $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->keterangan);

      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(14); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(16);

    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Barang Masuk Bulan ".$month_now);
    $excel->setActiveSheetIndex(0);
    // Proses file excel

    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    ob_end_clean();
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Barang Masuk '.$month_now.' '.$year.'.xlsx"'); // Set nama file excel nya
    $write->save('php://output');
  }

  //Download barang masuk


}

/* End of file traffic_data.php */
/* Location: ./application/controllers/traffic_data.php */