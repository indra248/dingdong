-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2018 at 07:20 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dingdong_warehouse`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(100) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `merek` varchar(100) DEFAULT NULL,
  `tipe` text,
  `size` varchar(20) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `kategori`, `merek`, `tipe`, `size`, `harga`) VALUES
('05CRP', 'AC', 'MITSUBISHI', '05 CRP', '1/2 P', 2700000),
('05FAR', 'AC', 'AUX', '05FAR', '1/2 P', 2225000),
('05FHR', 'AC', 'AUX', '05FHR', '1/2 P', 2225000),
('05GTO', 'AC', 'HAIER', '05GTO', '1/2 P', 2300000),
('05GTX', 'AC', 'HAIER', '05GTX', '1/2 P', 2300000),
('05GTZ', 'AC', 'HAIER', '05GTZ', ' 1/2 ', 2350000),
('05LXX', 'AC', 'HAIER', '05LXX', '1/2 P', 0),
('05NVA', 'AC', 'CHANGHONG', '05NVA-Indoor', '1/2 P', 2152000),
('05SAY', 'AC', 'SHARP', '05 SAY', '1/2 P', 2400000),
('05UCY', 'AC', 'SHARP', '05 UCY', '1/2 P', 2250000),
('07FAR', 'AC', 'AUX', '07FAR', '3/4 P', 2310000),
('07FCR', 'AC', 'AUX', '07FCR', '3/4', 2310000),
('07GT0', 'AC', 'HAIER', '07GT0', '1 PK', 2400000),
('07GTR', 'AC', 'HAIER', '07GTR', '3/4 P', 2400000),
('07GTX', 'AC', 'HAIER', '07GTX', '3/4 P', 2400000),
('07GTZ', 'AC', 'HAIER', '07GTZ', '3/4 P', 2450000),
('07NVA', 'AC', 'CHANGHONG', '07NVA', '3/4 P', 2241000),
('07NVB', 'AC', 'CHANGHONG', '07NVB', '3/4 P', 2277000),
('07UCY', 'AC', 'SHARP', '07 UCY', '3/4 P', 2350000),
('09CRP', 'AC', 'MITSUBISHI', '09 CRP', '1 PK', 2850000),
('09FAR', 'AC', 'AUX', '09FAR', '1 PK', 2400000),
('09FCR', 'AC', 'AUX', '09FCR', '1 PK', 2400000),
('09FHR', 'AC', 'AUX', '09FHR', '1 PK', 2400000),
('09FJR', 'AC', 'AUX', '09FJR', '1 PK', 2560000),
('09GTO', 'AC', 'HAIER', '09GT0', '1 PK', 2500000),
('09GTR', 'AC', 'HAIER', '09GTR', '1 PK', 2500000),
('09GTX', 'AC', 'HAIER', '09GTX', '1 PK', 2500000),
('09GTZ', 'AC', 'HAIER', '09GTZ', '1 PK', 2550000),
('09LEK', 'AC', 'HAIER', '09LEK', '1 PK', 2375000),
('09NVA', 'AC', 'CHANGHONG', '09NVA', '1 PK', 2331000),
('09SAY', 'AC', 'SHARP', '09 SAY', '1 PK', 2500000),
('09SEY', 'AC', 'SHARP', '09 SEY', '1 PK', 2625000),
('09UCY', 'AC', 'SHARP', '09 UCY', '1 PK', 2450000),
('09UEY', 'AC', 'SHARP', '09 UEY', '3/4 P', 2525000),
('100-AQA', 'CHEST FREEZER', 'AQUA', '100', '', 1860000),
('1080', 'MESIN CUCI', 'AQUA', '1080', '2 TABUNG', 1490000),
('122-FGT', 'CHEST FREEZER', 'FRIGIGATE', '122', '', 1700000),
('128-DMTS', 'CHEST FREEZER', 'DAIMITSU', '128', '', 1800000),
('1280', 'MESIN CUCI', 'AQUA', '1280', '2 TABUNG', 1825000),
('12CRP', 'AC', 'MITSUBISHI', '12 CRP', '1 1/2', 4035000),
('12ECO', 'AC', 'HAIER', '12ECO', '1/2 P', 0),
('12GTO', 'AC', 'HAIER', '12GTO', '1/2 P', 3450000),
('12GTR', 'AC', 'HAIER', '12GTR', '1/2 P', 3450000),
('12GTX', 'AC', 'HAIER', '12GTX', '1/2 P', 3300000),
('12GTZ', 'AC', 'HAIER', '12GTZ', '1/2 P', 3400000),
('12NVA', 'AC', 'CHANGHONG', '12NVA', '1/2 P', 3134000),
('12NVB', 'AC', 'CHANGHONG', '12NVB', '1/2 P', 3170000),
('12QB', 'AC', 'CHANGHONG', '12QB', '1/2 P', 0),
('12RKJ', 'AC', 'PANASONIC', '12 RKJ', '1 1/2', 4200000),
('12SEY', 'AC', 'SHARP', '12 SEY', '1 1/2', 4150000),
('12UCY', 'AC', 'SHARP', '12 UCY', '1 1/2', 3500000),
('140L', 'SHOWCASE', 'POLYTRON', '140L', '', 0),
('1480', 'MESIN CUCI', 'AQUA', '1480', '2 TABUNG', 2095000),
('151-D-FP', 'KULKAS', 'SHARP', '151-D-FP', '', 0),
('151-D-FW', 'KULKAS', 'SHARP', '151-D-FW', '', 0),
('153F', 'CHEST FREEZER', 'SANSIO', '153F', '', 0),
('15AGR', 'KULKAS', 'POLYTRON', '15AGR', '1', 1490000),
('15KTR', 'KULKAS', 'POLYTRON', '15KTR', '1', 1500000),
('160RC', 'KULKAS', 'POLYTRON', 'PRZ211', '2', 2380000),
('161GC', 'KULKAS', 'POLYTRON', '161GC', '1', 1500000),
('165-FB', 'KULKAS', 'SHARP', '165-FB', '', 0),
('165-FR', 'KULKAS', 'SHARP', '165-FR', '', 0),
('165MG', 'KULKAS', 'SHARP', '165MG', '', 0),
('166FB', 'KULKAS', 'SHARP', '166FB', '', 0),
('166FP', 'KULKAS', 'SHARP', '166FP', '', 0),
('166FW', 'KULKAS', 'SHARP', '166FW', '', 0),
('16AGB', 'KULKAS', 'POLYTRON', '16AGB', '1', 1610000),
('16AGR', 'KULKAS', 'POLYTRON', '16AGR', '1', 0),
('16AGV', 'KULKAS', 'POLYTRON', '16AGV', '', 0),
('16BER', 'KULKAS', 'POLYTRON', '16BER', '', 0),
('16BGV', 'KULKAS', 'POLYTRON', '16BGV', '', 0),
('16GH', 'FREEZER', 'PANASONIC', '16GH', '', 0),
('16LTV', 'KULKAS', 'POLYTRON', '16LTV', '', 0),
('170', 'KULKAS', 'SANKEN', '170', '', 0),
('171', 'KULKAS', 'SANKEN', '171', '1 PINTU', 0),
('172S', 'KULKAS', 'PANASONIC', '172S', '', 0),
('175', 'KULKAS', 'SANKEN', '175', '1 PINTU', 0),
('175FUS', 'KULKAS', 'SHARP', '175FUS', '', 0),
('179NF', 'KULKAS', 'PANASONIC', '179NF', '', 0),
('179NS', 'KULKAS', 'PANASONIC', '179NS', '', 0),
('179S', 'KULKAS', 'AQUA', '179S', '1', 1325000),
('180-FRG', 'SHOWCASE', 'FRIGIGATE', '180', '', 2125000),
('180B', 'KULKAS', 'POLYTRON', '180B', '', 0),
('180D', 'KULKAS', 'SHARP', '180D', '', 0),
('180L', 'SHOWCASE', 'POLYTRON', '180L', '', 0),
('181', 'KULKAS', 'SANKEN', '181', '1 PINTU', 0),
('181D', 'KULKAS', 'SHARP', '181D', '', 0),
('181L', 'SHOWCASE', 'POLYTRON', '181L', '', 0),
('185FB', 'KULKAS', 'SHARP', '185FB', '', 0),
('185GR', 'KULKAS', 'SHARP', '185GR', '', 0),
('185M', 'KULKAS', 'SHARP', '185M', '', 0),
('185SQB', 'KULKAS', 'LG', '185SQB', '', 0),
('185SQBB', 'KULKAS', 'LG', '185SQBB', '2', 2900000),
('187', 'KULKAS', 'AQUA', '187', '2 PIN', 1375000),
('18AGR', 'KULKAS', 'POLYTRON', '18AGR', '', 0),
('18AGV', 'KULKAS', 'POLYTRON', '18AGV', '', 0),
('18BGR', 'KULKAS', 'POLYTRON', '18BGR', '', 0),
('18ECO', 'AC', 'HAIER', '18ECO', '2 PK ', 0),
('18GTO', 'AC', 'HAIER', '18GTO', '2 PK', 0),
('18GTR', 'AC', 'HAIER', '18GTR', '2 PK', 4550000),
('18GTX', 'AC', 'HAIER', '18GTX', '2 PK', 4400000),
('18LEA', 'AC', 'HAIER', '18LEA', '2 PK ', 0),
('18LTR', 'KULKAS', 'POLYTRON', '18LTR', '', 0),
('18NVA', 'AC', 'CHANGHONG', '18NVA', '2 PK', 4231000),
('18RKP', 'AC', 'PANASONIC', '18 RKP', '2 PK', 5400000),
('18UCY', 'AC', 'SHARP', '18 UCY', '2 PK', 4800000),
('190D', 'KULKAS', 'SHARP', '190D', '', 0),
('190DS', 'KULKAS', 'AQUA', '190DS', '1', 1400000),
('190LS', 'KULKAS', 'AQUA', '190S', '1', 1400000),
('191D', 'KULKAS', 'SHARP', '191D', '', 0),
('195M-SR', 'KULKAS', 'SHARP', '195M-SR', '', 0),
('195MD', 'KULKAS', 'SHARP', '195MD', '', 0),
('196-ND-FB', 'KULKAS', 'SHARP', '196-ND-FB', '', 0),
('196ND', 'KULKAS', 'SHARP', '196ND', '', 0),
('198G', 'KULKAS', 'PANASONIC', '198G', '', 0),
('199N', 'KULKAS', 'PANASONIC', '199N', '', 0),
('19M300BGS', 'KULKAS', 'SAMSUNG', '19M300BGS', '', 0),
('200-AQA', 'CHEST FREEZER', 'AQUA', '200', '', 2320000),
('200-FGT', 'CHEST FREEZER', 'FRIGIGATE', '200', '', 2050000),
('200-FRG', 'SHOWCASE', 'FRIGIGATE', '200', '', 2225000),
('200-SHP', 'CHEST FREEZER', 'SHARP', '200', '', 0),
('202RL', 'KULKAS', 'LG', '202RL', '', 0),
('202SLC', 'KULKAS', 'LG', '202SLC', '', 0),
('208', 'SHOWCASE', 'DAIMITSU', '208', '4 RAK', 2600000),
('20FAR', 'KULKAS', 'SAMSUNG', '20FAR', '', 0),
('210-FGT', 'CHEST FREEZER', 'FRIGIGATE', '210', '', 2370000),
('210SHARP', 'SHOWCASE', 'SHARP', '210', '', 0),
('212SLK', 'KULKAS', 'LG', '212SLK', '', 0),
('219', 'SHOWCASE', 'DAIMITSU', '219', '', 2500000),
('21AGV', 'KULKAS', 'POLYTRON', '21AGV', '', 0),
('21KTR', 'KULKAS', 'POLYTRON', '21KTR', '', 0),
('220', 'KULKAS', 'SANKEN', '220', '', 0),
('221', 'KULKAS', 'SANKEN', '221', '', 0),
('228', 'SHOWCASE', 'DAIMITSU', '228', '5 RAK', 2875000),
('229H', 'KULKAS', 'PANASONIC', '229H', '2 PIN', 0),
('229N', 'KULKAS', 'PANASONIC', '229N', '', 0),
('229S', 'KULKAS', 'PANASONIC', '229S', '', 0),
('22FAS', 'KULKAS', 'SAMSUNG', '22FAS', '', 0),
('22NA', 'KULKAS', 'PANASONIC', '22NA', '', 0),
('230L', 'SHOWCASE', 'POLYTRON', '230L', '', 0),
('231', 'KULKAS', 'SANKEN', '231', '2 PINTU', 0),
('231L', 'SHOWCASE', 'POLYTRON', '231L', '', 0),
('233D', 'SHOWCASE', 'POLYTRON', '233D', '', 0),
('235GC', 'KULKAS', 'SHARP', '235GC', '', 0),
('236ND', 'KULKAS', 'SHARP', '236ND', '', 0),
('236ND-FB', 'KULKAS', 'SHARP', '236ND-FB', '', 0),
('236ND-FW', 'KULKAS', 'SHARP', '236ND-FW', '', 0),
('238-CF', 'CHEST FREEZER', 'DAIMITSU', '238', '', 2850000),
('239FW', 'KULKAS', 'AQUA', '239FW', '2 PIN', 2350000),
('23BGR', 'KULKAS', 'POLYTRON', '23BGR', '', 0),
('23BGV', 'KULKAS', 'POLYTRON', '23BGV', '', 0),
('23QN', 'KULKAS', 'POLYTRON', '23QN', '2', 3125000),
('23QS', 'KULKAS', 'POLYTRON', '23QS', '2', 3125000),
('23QV', 'KULKAS', 'POLYTRON', '23QV', '2', 3125000),
('240RSA', 'SHOWCASE', 'RSA', '240', '', 0),
('246-XG', 'KULKAS', 'SHARP', '246-XG', '', 0),
('250-FRG', 'SHOWCASE', 'FRIGIGATE', '250', '', 2550000),
('259FS', 'KULKAS', 'AQUA', '259FS', '2', 2450000),
('259FW', 'KULKAS', 'AQUA', '259FW', '2', 2450000),
('25BG', 'KULKAS', 'MITSUBISHI', '25BG', '', 0),
('25FAR', 'KULKAS', 'SAMSUNG', '25FAR', '', 0),
('25QB', 'KULKAS', 'POLYTRON', '25QB', '', 0),
('25QN', 'KULKAS', 'POLYTRON', '25QN', '', 0),
('260DS', 'KULKAS', 'AQUA', '260DS', '2', 2275000),
('260S', 'KULKAS', 'AQUA', '260S', '2', 2275000),
('269H', 'KULKAS', 'PANASONIC', '269H', '', 0),
('269N', 'KULKAS', 'PANASONIC', '269N', '', 0),
('269S', 'KULKAS', 'PANASONIC', '269S', '', 0),
('270DS', 'KULKAS', 'AQUA', '270DS', '2', 2325000),
('270LS', 'KULKAS', 'AQUA', '270LS', '2', 2325000),
('275RBK', 'KULKAS', 'AQUA', '275RBK', '2', 2985000),
('278', 'SHOWCASE', 'DAIMITSU', '278', '', 3550000),
('278BK', 'SHOWCASE', 'SANKEN', '278BK', '1', 3000000),
('28QV', 'KULKAS', 'POLYTRON', '28QV', '2', 3075000),
('300-FGT', 'CHEST FREEZER', 'FRIGIGATE', '300-FGT', '', 2375000),
('300-SHP', 'CHEST FREEZER', 'SHARP', '300', '', 0),
('300RSA', 'SHOWCASE', 'RSA', '300', '', 0),
('304', 'FREEZER', 'LG', '304', '', 0),
('316ND', 'KULKAS', 'SHARP', '316ND', '', 0),
('320-SKN', 'CHEST FREEZER', 'SANKEN', '320', '', 0),
('328', 'SHOWCASE', 'DAIMITSU', '328', '6  RAK', 3850000),
('328-CF', 'CHEST FREEZER', 'DAIMITSU', '328', '', 2650000),
('350', 'KULKAS', 'SANKEN', '350', '2 PINTU', 0),
('426', 'KULKAS', 'SHARP', '426', '', 0),
('428-CF', 'CHEST FREEZER', 'DAIMITSU', '428', '', 3575000),
('450FW', 'KULKAS', 'SHARP', '450FW', '', 0),
('450SD', 'KULKAS', 'SHARP', '450SD', '', 0),
('488-CF', 'CHEST FREEZER', 'DAIMITSU', '488', '', 3650000),
('52-420-GP', 'KULKAS', 'SHARP', '52-420-GP', '', 0),
('528-CF', 'CHEST FREEZER', 'DAIMITSU', '528', '', 4600000),
('5SKJ', 'AC', 'PANASONIC', 'YN5SKJ', '1/2 P', 2675000),
('628-CF', 'CHEST FREEZER', 'DAIMITSU', '628', '', 5250000),
('740', 'MESIN CUCI', 'AQUA', '740', '2 tabung', 1070000),
('755', 'MESIN CUCI', 'AQUA', '755', '2 TABUNG', 1090000),
('771', 'MESIN CUCI', 'AQUA', '771', '2 tabung', 1060000),
('780', 'MESIN CUCI', 'AQUA', '780', '2 tabung', 1100000),
('781', 'MESIN CUCI', 'AQUA', '781', '2 tabung', 1090000),
('7PKP', 'AC', 'PANASONIC', '7 PKP', '3/4 P', 3500000),
('7SKJ', 'AC', 'PANASONIC', '7 SKJ', '3/4 P', 2650000),
('870', 'MESIN CUCI', 'AQUA', '870', '2 tabung', 1178000),
('871', 'MESIN CUCI', 'AQUA', '871', '2 tabung', 1150000),
('87DH', 'MESIN CUCI', 'AQUA', '87DH', '1 TABUNG', 1900000),
('880', 'MESIN CUCI', 'AQUA', '880', '2 TABUNG', 1170000),
('881', 'MESIN CUCI', 'AQUA', '881', '2 TABUNG', 1170000),
('89XT', 'MESIN CUCI', 'AQUA', '89XT', '1 TABUNG', 1955000),
('97DH', 'MESIN CUCI', 'AQUA', '97DH', '1 TABUNG', 2050000),
('980', 'MESIN CUCI', 'AQUA', '980', '2 TABUNG', 1370000),
('98DD', 'MESIN CUCI', 'AQUA', '98DD', '1 TABUNG', 2075000),
('99XT', 'MESIN CUCI', 'AQUA', '99XT', '1 TABUNG', 2140000),
('9QA', 'AC', 'CHANGHONG', '9QA', '1 PK', 0),
('9SKJ', 'AC', 'PANASONIC', '9 SKJ', '1 PK', 2915000),
('9TKJ', 'AC', 'PANASONIC', '9 TKJ', '1 PK', 2950000),
('GNB2005QBB', 'KULKAS', 'LG', 'GNB2005QBB', '', 0),
('GNC-272-SLGN', 'KULKAS', 'LG', 'GNC-272-SLGN', '2', 4600000),
('KB001', 'POMPA AIR', 'COBA SATU', 'MELEDAK', '', 12500000),
('PAS-28', 'SPEAKER', 'POLYTRON', 'SPEAKER AKTIF PAS-28', '', 0),
('PAS-68', 'SPEAKER', 'POLYTRON ', 'SP AKTIF PAS-28', '', 0),
('PR72115P', 'KULKAS', 'POLYTRON', 'PR72115P', '', 0),
('PRB-211G', 'KULKAS', 'POLYTRON', 'PRB-211G', '', 0),
('PRM-21QB', 'KULKAS', 'POLYTRON', 'PRM-21QB', '', 0),
('PRM-21QV', 'KULKAS', 'POLYTRON', 'PRM-21QV', '', 0),
('PRM-21QX', 'KULKAS', 'POLYTRON', 'PRM-21QX', '', 0),
('PRO-15ELR', 'KULKAS', 'POLYTRON', 'PRO-15ELR', '', 0),
('PRZ211', 'KULKAS', 'POLYTRON', 'PRZ211', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `kbarang_keluar` bigint(20) NOT NULL,
  `petugas_admin` varchar(100) NOT NULL,
  `petugas_lapangan` varchar(100) DEFAULT NULL,
  `tanggal_keluar_barang` date DEFAULT NULL,
  `kode_barang` varchar(100) DEFAULT NULL,
  `banyak_barang` bigint(20) DEFAULT NULL,
  `id_lokasi` varchar(20) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `barang_keluar`
--

INSERT INTO `barang_keluar` (`kbarang_keluar`, `petugas_admin`, `petugas_lapangan`, `tanggal_keluar_barang`, `kode_barang`, `banyak_barang`, `id_lokasi`, `keterangan`) VALUES
(1, 'Administrator', 'KOMAR', '2018-05-05', 'KB001', 3, 'TK-MM', '34343'),
(2, 'Administrator', '', '2018-05-05', 'KB001', 3, 'TK-MM', '43434'),
(3, 'Administrator', 'EVAN', '2018-05-03', 'KB001', 3, 'TK-MM', 'ERER'),
(4, 'Administrator', 'HENDRA', '2018-05-05', 'KB001', 3, 'TK-MM', 'RERDSDSKDJSKDJSKDJSKDJSKDJSKDJSD');

--
-- Triggers `barang_keluar`
--
DELIMITER $$
CREATE TRIGGER `decrease_stok` AFTER INSERT ON `barang_keluar` FOR EACH ROW BEGIN
	UPDATE lokasi_barang SET stok_barang = stok_barang - new.banyak_barang 
	WHERE kode_barang = new.kode_barang AND id_lokasi = new.id_lokasi;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `kbarang_masuk` bigint(20) NOT NULL,
  `kode_barang` varchar(100) DEFAULT NULL,
  `tanggal_masuk_barang` date DEFAULT NULL,
  `banyak_barang` bigint(20) NOT NULL,
  `id_lokasi` varchar(20) NOT NULL,
  `id_karyawan` varchar(20) NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`kbarang_masuk`, `kode_barang`, `tanggal_masuk_barang`, `banyak_barang`, `id_lokasi`, `id_karyawan`, `keterangan`) VALUES
(1, 'KB001', '2018-05-05', 12, 'TK-MM', 'ADM-001', 'TES AJA'),
(2, 'KB001', '2018-05-05', 3, 'GD-1', 'ADM-001', '');

--
-- Triggers `barang_masuk`
--
DELIMITER $$
CREATE TRIGGER `manytomany` AFTER INSERT ON `barang_masuk` FOR EACH ROW BEGIN
	update lokasi_barang set stok_barang = stok_barang + new.banyak_barang 
	where kode_barang = new.kode_barang and id_lokasi = new.id_lokasi;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` varchar(100) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nomor_telepon` varchar(15) DEFAULT NULL,
  `posisi` varchar(30) DEFAULT NULL,
  `nama_admin` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level_user` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `nama`, `nomor_telepon`, `posisi`, `nama_admin`, `password`, `level_user`) VALUES
('ADM-001', 'Administrator', '081294208796', 'Administrator', 'Administrator', 'Administrator', '2'),
('ADM-002', 'Agus', NULL, 'Lapangan', 'agustoko', 'agus1822', '3'),
('ADM-003', 'HENDRA', NULL, 'Admin', 'hendratoko', 'hendra182234', '2'),
('ADM-004', 'KOMAR', NULL, 'Admin', 'komartoko', NULL, NULL),
('ADM-005', 'WARTINI', NULL, 'Admin', 'wartinitoko', 'wartini1822', '2'),
('LPG-001', 'EVAN', NULL, 'Lapangan', NULL, NULL, NULL),
('LPG-002', 'Wawan', NULL, 'Lapangan', NULL, NULL, NULL),
('LPG-003', 'AHMAD', NULL, 'Lapangan', NULL, NULL, NULL),
('LPG-004', 'ARI', NULL, 'Lapangan', NULL, NULL, NULL),
('LPG-005', 'NANDI', NULL, 'Lapangan', NULL, NULL, NULL),
('LPG-006', 'BIAN', NULL, 'Lapangan', NULL, NULL, NULL),
('LPG-008', 'YANTO', NULL, 'Lapangan', NULL, NULL, NULL),
('LPG-009', 'TOHIR', NULL, 'Lapangan', NULL, NULL, NULL),
('LPGG-007', 'KUNCORO', NULL, 'Lapangan', NULL, NULL, NULL),
('OWN-001', 'OWNER', NULL, 'Owner', 'dingdong', 'owner', '3');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` bigint(20) NOT NULL,
  `nama_kategori` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(5, 'TV'),
(6, 'KULKAS'),
(7, 'KIPAS ANGIN'),
(8, 'SPEAKER'),
(9, 'MICROWAVE'),
(10, 'REMOTE'),
(11, 'MIC BLOETOOTH'),
(12, 'AC'),
(13, 'AMPLIFIER'),
(14, 'BLENDER'),
(15, 'POMPA AIR '),
(16, 'ANTENNA'),
(17, 'DVD'),
(18, 'TOASTER'),
(19, 'OVEN'),
(20, 'JUICER'),
(21, 'MIXER'),
(22, 'MAGIC COM'),
(23, 'RADIO'),
(24, 'PESAWAT TELEPON'),
(25, 'HOME THEATER'),
(26, 'DISPENSER'),
(27, 'RICE BOX'),
(28, 'SETRIKA'),
(29, 'TECO LISTRIK'),
(30, 'RECEIVER'),
(31, 'PANCI PRESTO'),
(32, 'LAMPU EMERGENCY'),
(33, 'BRACKET'),
(34, 'VACUM CLEANER'),
(35, 'KOMPOR'),
(36, 'SELANG & REGULATOR'),
(37, 'CALCULATOR'),
(38, 'RODA KULKAS'),
(39, 'TABUNG GAS'),
(40, 'CORONG'),
(41, 'MEGAPHONE'),
(42, 'COOPER'),
(43, 'SHOWCASE'),
(44, 'CHEST FREEZER'),
(45, 'FREEZER'),
(46, 'MESIN CUCI'),
(47, 'STABILIZER'),
(48, 'AIR COOLER'),
(49, 'BOGI BOX'),
(50, 'KAMERA');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi_barang`
--

CREATE TABLE `lokasi_barang` (
  `kstok_lokasi_barang` bigint(20) NOT NULL,
  `id_lokasi` varchar(20) DEFAULT NULL,
  `kode_barang` varchar(100) DEFAULT NULL,
  `stok_barang` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lokasi_barang`
--

INSERT INTO `lokasi_barang` (`kstok_lokasi_barang`, `id_lokasi`, `kode_barang`, `stok_barang`) VALUES
(48, 'TK-MM', '05UCY', 0),
(49, 'TK-PP', '05UCY', 0),
(50, 'GD-2', '05UCY', 0),
(51, 'GD-1', '18NVA', 0),
(52, 'GD-1', '05NVA', 0),
(53, 'GD-1', '07NVA', 0),
(54, 'GD-1', '9QA', 0),
(55, 'GD-1', '12QB', 0),
(57, 'GD-1', '07NVB', 0),
(58, 'GD-1', '09NVA', 0),
(59, 'GD-1', '12NVA', 0),
(60, 'GD-1', '12NVB', 0),
(61, 'GD-1', '05CRP', 0),
(62, 'GD-1', '09CRP', 0),
(63, 'GD-1', '12CRP', 0),
(64, 'GD-1', '5SKJ', 0),
(65, 'GD-1', '7PKP', 0),
(66, 'GD-1', '7SKJ', 0),
(67, 'GD-1', '9SKJ', 0),
(68, 'GD-1', '9TKJ', 0),
(69, 'GD-1', '12RKJ', 0),
(70, 'GD-1', '18RKP', 0),
(71, 'GD-1', '05SAY', 0),
(72, 'GD-1', '07UCY', 0),
(73, 'GD-1', '09SAY', 0),
(74, 'GD-1', '09SEY', 0),
(75, 'GD-1', '09UCY', 0),
(76, 'GD-1', '09UEY', 0),
(77, 'GD-1', '12SEY', 0),
(78, 'GD-1', '12UCY', 0),
(79, 'GD-1', '18UCY', 0),
(80, 'GD-1', '05FAR', 0),
(81, 'GD-2', '05FAR', 0),
(82, 'GD-1', '07FAR', 0),
(83, 'GD-2', '07FAR', 0),
(84, 'GD-1', '09FAR', 0),
(85, 'GD-2', '09FAR', 0),
(86, 'GD-1', '05FHR', 0),
(87, 'GD-2', '05FHR', 0),
(89, 'GD-1', '09FHR', 0),
(90, 'GD-2', '09FHR', 0),
(91, 'GD-1', '07FCR', 0),
(92, 'GD-2', '07FCR', 0),
(93, 'GD-1', '09FJR', 0),
(94, 'GD-2', '09FJR', 0),
(95, 'GD-1', '09FCR', 0),
(96, 'GD-2', '09FCR', 0),
(97, 'GD-1', '05GTO', 0),
(98, 'GD-2', '05GTO', 0),
(99, 'TK-MM', '05GTO', 0),
(100, 'TK-PP', '05GTO', 0),
(101, 'GD-1', '07GT0', 0),
(102, 'GD-2', '07GT0', 0),
(103, 'TK-MM', '07GT0', 0),
(104, 'TK-PP', '07GT0', 0),
(105, 'GD-1', '09GTO', 0),
(106, 'GD-2', '09GTO', 0),
(107, 'TK-MM', '09GTO', 0),
(108, 'TK-PP', '09GTO', 0),
(109, 'GD-1', '12GTO', 0),
(110, 'TK-MM', '12GTO', 0),
(111, NULL, NULL, 0),
(112, 'GD-2', '12GTO', 0),
(113, 'TK-PP', '12GTO', 0),
(114, 'GD-1', '18GTO', 0),
(115, 'GD-2', '18GTO', 0),
(116, 'TK-MM', '18GTO', 0),
(117, 'TK-PP', '18GTO', 0),
(123, 'GD-1', '07GTR', 0),
(124, 'GD-2', '07GTR', 0),
(125, 'TK-MM', '07GTR', 0),
(126, 'TK-PP', '07GTR', 0),
(127, 'GD-1', '09GTR', 0),
(128, 'GD-2', '09GTR', 0),
(129, 'TK-MM', '09GTR', 0),
(130, 'TK-PP', '09GTR', 0),
(131, 'GD-1', '12GTR', 0),
(132, 'GD-2', '12GTR', 0),
(133, 'TK-MM', '12GTR', 0),
(134, 'TK-PP', '12GTR', 0),
(135, 'GD-1', '18GTR', 0),
(136, 'GD-2', '18GTR', 0),
(137, 'TK-MM', '18GTR', 0),
(138, 'TK-PP', '18GTR', 0),
(139, 'GD-1', '12ECO', 0),
(140, 'GD-2', '12ECO', 0),
(141, 'TK-MM', '12ECO', 0),
(142, 'TK-PP', '12ECO', 0),
(143, 'GD-1', '18ECO', 0),
(144, 'GD-2', '18ECO', 0),
(145, 'TK-MM', '18ECO', 0),
(146, 'TK-PP', '18ECO', 0),
(147, 'GD-1', '18LEA', 0),
(148, 'GD-2', '18LEA', 0),
(149, 'TK-MM', '18LEA', 0),
(150, 'TK-PP', '18LEA', 0),
(151, 'GD-1', '05GTX', 0),
(152, 'GD-2', '05GTX', 0),
(153, 'TK-MM', '05GTX', 0),
(154, 'TK-PP', '05GTX', 0),
(155, 'GD-1', '07GTX', 0),
(156, 'GD-2', '07GTX', 0),
(157, 'TK-MM', '07GTX', 0),
(158, 'TK-PP', '07GTX', 0),
(162, 'GD-1', '09GTX', 0),
(163, 'GD-2', '09GTX', 0),
(164, 'TK-MM', '09GTX', 0),
(165, 'TK-PP', '09GTX', 0),
(166, 'GD-1', '05LXX', 0),
(167, 'GD-2', '05LXX', 0),
(168, 'TK-MM', '05LXX', 0),
(169, 'TK-PP', '05LXX', 0),
(170, 'GD-1', '05GTZ', 0),
(171, 'GD-2', '05GTZ', 0),
(172, 'TK-MM', '05GTZ', 0),
(173, 'TK-PP', '05GTZ', 0),
(174, 'GD-1', '07GTZ', 0),
(175, 'GD-2', '07GTZ', 0),
(176, 'TK-MM', '07GTZ', 0),
(177, 'TK-PP', '07GTZ', 0),
(179, 'GD-1', '09GTZ', 0),
(180, 'GD-2', '09GTZ', 0),
(181, 'TK-MM', '09GTZ', 0),
(182, 'TK-PP', '09GTZ', 0),
(183, 'GD-1', '09LEK', 0),
(184, 'GD-2', '09LEK', 0),
(185, 'TK-MM', '09LEK', 0),
(186, 'TK-PP', '09LEK', 0),
(187, 'GD-1', '12GTZ', 0),
(188, 'GD-2', '12GTZ', 0),
(189, 'TK-MM', '12GTZ', 0),
(190, 'TK-PP', '12GTZ', 0),
(195, 'GD-1', '12GTX', 0),
(196, 'GD-2', '12GTX', 0),
(197, 'TK-MM', '12GTX', 0),
(198, 'TK-PP', '12GTX', 0),
(199, 'GD-1', '18GTX', 0),
(200, 'GD-2', '18GTX', 0),
(201, 'TK-MM', '18GTX', 0),
(202, 'TK-PP', '18GTX', 0),
(203, 'TK-MM', 'PAS-28', 0),
(204, 'TK-PP', 'PAS-28', 0),
(205, 'GD-1', 'PAS-28', 0),
(206, 'GD-2', 'PAS-28', 0),
(207, 'GD-1', 'PAS-68', 0),
(208, 'GD-2', 'PAS-68', 0),
(209, 'TK-MM', 'PAS-68', 0),
(210, 'TK-PP', 'PAS-68', 0),
(211, 'GD-1', '181D', 0),
(212, 'GD-2', '181D', 0),
(213, 'TK-MM', '181D', 0),
(214, 'TK-PP', '181D', 0),
(215, 'GD-1', '190D', 0),
(216, 'GD-2', '190D', 0),
(217, 'TK-MM', '190D', 0),
(218, 'TK-PP', '190D', 0),
(219, 'GD-1', '191D', 0),
(220, 'GD-2', '191D', 0),
(221, 'TK-MM', '191D', 0),
(222, 'TK-PP', '191D', 0),
(224, 'GD-1', '195MD', 0),
(225, 'GD-2', '195MD', 0),
(226, 'TK-MM', '195MD', 0),
(227, 'TK-PP', '195MD', 0),
(229, NULL, NULL, 0),
(231, 'GD-1', '195M-SR', 0),
(232, 'GD-2', '195M-SR', 0),
(233, 'TK-MM', '195M-SR', 0),
(234, 'TK-PP', '195M-SR', 0),
(235, 'GD-1', '196ND', 0),
(236, 'GD-2', '196ND', 0),
(237, 'TK-MM', '196ND', 0),
(238, 'TK-PP', '196ND', 0),
(239, 'GD-1', '316ND', 0),
(240, 'GD-2', '316ND', 0),
(241, 'TK-MM', '316ND', 0),
(242, 'TK-PP', '316ND', 0),
(243, 'GD-1', '236ND', 0),
(244, 'GD-2', '236ND', 0),
(246, 'TK-MM', '236ND', 0),
(247, 'TK-PP', '236ND', 0),
(248, 'GD-1', '185M', 0),
(249, 'GD-2', '185M', 0),
(250, 'TK-MM', '185M', 0),
(251, 'TK-PP', '185M', 0),
(252, 'GD-1', '185FB', 0),
(254, 'GD-2', '185FB', 0),
(255, 'TK-MM', '185FB', 0),
(256, 'TK-PP', '185FB', 0),
(257, 'GD-1', '185GR', 0),
(258, 'GD-2', '185GR', 0),
(259, 'TK-MM', '185GR', 0),
(260, 'TK-PP', '185GR', 0),
(261, 'GD-1', '180D', 0),
(262, 'GD-2', '180D', 0),
(263, 'TK-MM', '180D', 0),
(264, 'TK-PP', '180D', 0),
(265, 'GD-1', '166FP', 0),
(266, 'GD-2', '166FP', 0),
(267, 'TK-MM', '166FP', 0),
(268, 'TK-PP', '166FP', 0),
(269, 'GD-1', '426', 0),
(270, 'GD-2', '426', 0),
(271, 'TK-MM', '426', 0),
(272, 'TK-PP', '426', 0),
(273, 'GD-1', '235GC', 0),
(274, 'GD-2', '235GC', 0),
(275, 'TK-MM', '235GC', 0),
(276, 'TK-PP', '235GC', 0),
(277, 'GD-1', '165MG', 0),
(278, 'GD-2', '165MG', 0),
(279, 'TK-MM', '165MG', 0),
(280, 'TK-PP', '165MG', 0),
(281, 'GD-1', '450FW', 0),
(282, 'GD-2', '450FW', 0),
(283, 'TK-MM', '450FW', 0),
(284, 'TK-PP', '450FW', 0),
(285, 'GD-1', '450SD', 0),
(286, 'GD-2', '450SD', 0),
(287, 'TK-MM', '450SD', 0),
(288, 'TK-PP', '450SD', 0),
(289, 'GD-1', '52-420-GP', 0),
(290, 'GD-2', '52-420-GP', 0),
(291, 'TK-MM', '52-420-GP', 0),
(292, 'TK-PP', '52-420-GP', 0),
(293, 'GD-1', '175FUS', 0),
(294, 'GD-2', '175FUS', 0),
(295, 'TK-MM', '175FUS', 0),
(296, 'TK-PP', '175FUS', 0),
(297, 'GD-1', '166FB', 0),
(298, 'GD-2', '166FB', 0),
(299, 'TK-MM', '166FB', 0),
(300, 'TK-PP', '166FB', 0),
(301, 'GD-1', '166FW', 0),
(302, 'GD-2', '166FW', 0),
(303, 'TK-MM', '166FW', 0),
(304, 'TK-PP', '166FW', 0),
(305, 'GD-1', '196-ND-FB', 0),
(306, 'GD-2', '196-ND-FB', 0),
(307, 'TK-MM', '196-ND-FB', 0),
(308, 'TK-PP', '196-ND-FB', 0),
(309, 'GD-1', '151-D-FP', 0),
(310, 'GD-2', '151-D-FP', 0),
(311, 'TK-MM', '151-D-FP', 0),
(312, 'TK-PP', '151-D-FP', 0),
(313, 'GD-1', '236ND-FW', 0),
(314, 'GD-2', '236ND-FW', 0),
(315, 'TK-MM', '236ND-FW', 0),
(316, 'TK-PP', '236ND-FW', 0),
(317, 'GD-1', '236ND-FB', 0),
(318, 'GD-2', '236ND-FB', 0),
(319, 'TK-MM', '236ND-FB', 0),
(320, 'TK-PP', '236ND-FB', 0),
(321, 'GD-1', '165-FR', 0),
(322, 'GD-2', '165-FR', 0),
(323, 'TK-MM', '165-FR', 0),
(324, 'TK-PP', '165-FR', 0),
(329, 'GD-1', '246-XG', 0),
(330, 'GD-2', '246-XG', 0),
(331, 'TK-MM', '246-XG', 0),
(332, 'TK-PP', '246-XG', 0),
(333, 'GD-1', '179S', 0),
(334, 'GD-2', '179S', 0),
(335, 'TK-MM', '179S', 0),
(336, 'TK-PP', '179S', 0),
(337, 'GD-1', '187', 0),
(338, 'GD-2', '187', 0),
(339, 'TK-MM', '187', 0),
(340, 'TK-PP', '187', 0),
(341, 'GD-1', '239FW', 0),
(342, 'GD-2', '239FW', 0),
(343, 'TK-MM', '239FW', 0),
(344, 'TK-PP', '239FW', 0),
(345, 'GD-1', '259FW', 0),
(346, 'GD-2', '259FW', 0),
(347, 'TK-MM', '259FW', 0),
(348, 'TK-PP', '259FW', 0),
(349, 'GD-1', '270LS', 0),
(350, 'GD-2', '270LS', 0),
(351, 'TK-MM', '270LS', 0),
(352, 'TK-PP', '270LS', 0),
(353, 'GD-1', '270DS', 0),
(354, 'GD-2', '270DS', 0),
(355, 'TK-MM', '270DS', 0),
(356, 'TK-PP', '270DS', 0),
(357, 'GD-1', '259FS', 0),
(358, 'GD-2', '259FS', 0),
(359, 'TK-MM', '259FS', 0),
(360, 'TK-PP', '259FS', 0),
(361, 'GD-1', '260S', 0),
(362, 'GD-2', '260S', 0),
(363, 'TK-MM', '260S', 0),
(364, 'TK-PP', '260S', 0),
(365, 'GD-1', '190LS', 0),
(366, 'GD-2', '190LS', 0),
(367, 'TK-MM', '190LS', 0),
(368, 'TK-PP', '190LS', 0),
(369, 'GD-1', '275RBK', 0),
(370, 'GD-2', '275RBK', 0),
(371, 'TK-MM', '275RBK', 0),
(372, 'TK-PP', '275RBK', 0),
(373, 'GD-1', '190DS', 0),
(374, 'GD-2', '190DS', 0),
(375, 'TK-MM', '190DS', 0),
(376, 'TK-PP', '190DS', 0),
(377, 'GD-1', '260DS', 0),
(378, 'GD-2', '260DS', 0),
(379, 'TK-MM', '260DS', 0),
(380, 'TK-PP', '260DS', 0),
(381, 'GD-1', '15AGR', 0),
(382, 'GD-2', '15AGR', 0),
(383, 'TK-MM', '15AGR', 0),
(384, 'TK-PP', '15AGR', 0),
(385, 'GD-1', '15KTR', 0),
(386, 'GD-2', '15KTR', 0),
(387, 'TK-MM', '15KTR', 0),
(388, 'TK-PP', '15KTR', 0),
(389, 'GD-1', '16AGB', 0),
(390, 'GD-2', '16AGB', 0),
(391, 'TK-MM', '16AGB', 0),
(392, 'TK-PP', '16AGB', 0),
(393, 'GD-1', '16AGR', 0),
(394, 'GD-2', '16AGR', 0),
(395, 'TK-MM', '16AGR', 0),
(396, 'TK-PP', '16AGR', 0),
(397, 'GD-1', '16BER', 0),
(398, 'GD-2', '16BER', 0),
(399, 'TK-MM', '16BER', 0),
(400, 'TK-PP', '16BER', 0),
(401, 'GD-1', '16BGV', 0),
(402, 'GD-2', '16BGV', 0),
(403, 'TK-MM', '16BGV', 0),
(404, 'TK-PP', '16BGV', 0),
(405, 'GD-1', '18BGR', 0),
(406, 'GD-2', '18BGR', 0),
(407, 'TK-MM', '18BGR', 0),
(408, 'TK-PP', '18BGR', 0),
(409, 'GD-1', '18LTR', 0),
(410, 'GD-2', '18LTR', 0),
(411, 'TK-MM', '18LTR', 0),
(412, 'TK-PP', '18LTR', 0),
(413, 'GD-1', '18AGR', 0),
(414, 'GD-2', '18AGR', 0),
(415, 'TK-MM', '18AGR', 0),
(416, 'TK-PP', '18AGR', 0),
(417, 'GD-1', '18AGV', 0),
(418, 'GD-2', '18AGV', 0),
(419, 'TK-MM', '18AGV', 0),
(420, 'TK-PP', '18AGV', 0),
(421, 'GD-1', '23BGV', 0),
(422, 'GD-2', '23BGV', 0),
(423, 'TK-MM', '23BGV', 0),
(424, 'TK-PP', '23BGV', 0),
(425, 'GD-1', '23QS', 0),
(426, 'GD-2', '23QS', 0),
(427, 'TK-MM', '23QS', 0),
(428, 'TK-PP', '23QS', 0),
(429, 'GD-1', '23QN', 0),
(430, 'GD-2', '23QN', 0),
(431, 'TK-MM', '23QN', 0),
(432, 'TK-PP', '23QN', 0),
(433, 'GD-1', '23QV', 0),
(434, 'GD-2', '23QV', 0),
(435, 'TK-MM', '23QV', 0),
(436, 'TK-PP', '23QV', 0),
(437, 'GD-1', '21KTR', 0),
(438, 'GD-2', '21KTR', 0),
(439, 'TK-MM', '21KTR', 0),
(440, 'TK-PP', '21KTR', 0),
(441, 'GD-1', '25QN', 0),
(442, 'GD-2', '25QN', 0),
(443, 'TK-MM', '25QN', 0),
(444, 'TK-PP', '25QN', 0),
(445, 'GD-1', '25QB', 0),
(446, 'GD-2', '25QB', 0),
(447, 'TK-MM', '25QB', 0),
(448, 'TK-PP', '25QB', 0),
(449, 'GD-1', '28QV', 0),
(450, 'GD-2', '28QV', 0),
(451, 'TK-MM', '28QV', 0),
(452, 'TK-PP', '28QV', 0),
(453, 'GD-1', '180B', 0),
(454, 'GD-2', '180B', 0),
(455, 'TK-MM', '180B', 0),
(456, 'TK-PP', '180B', 0),
(457, 'GD-1', '161GC', 0),
(458, 'GD-2', '161GC', 0),
(459, 'TK-MM', '161GC', 0),
(460, 'TK-PP', '161GC', 0),
(461, 'GD-1', '21AGV', 0),
(462, 'GD-2', '21AGV', 0),
(463, 'TK-MM', '21AGV', 0),
(464, 'TK-PP', '21AGV', 0),
(465, 'GD-1', 'PRZ211', 0),
(466, 'GD-2', 'PRZ211', 0),
(467, 'TK-MM', 'PRZ211', 0),
(468, 'TK-PP', 'PRZ211', 0),
(469, 'GD-1', 'PRM-21QV', 0),
(470, 'GD-2', 'PRM-21QV', 0),
(471, 'TK-MM', 'PRM-21QV', 0),
(472, 'TK-PP', 'PRM-21QV', 0),
(473, 'GD-1', 'PRM-21QX', 0),
(474, 'GD-2', 'PRM-21QX', 0),
(475, 'TK-MM', 'PRM-21QX', 0),
(476, 'TK-PP', 'PRM-21QX', 0),
(477, 'GD-1', 'PRM-21QB', 0),
(478, 'GD-2', 'PRM-21QB', 0),
(479, 'TK-MM', 'PRM-21QB', 0),
(480, 'TK-PP', 'PRM-21QB', 0),
(481, 'GD-1', 'PRB-211G', 0),
(482, 'GD-2', 'PRB-211G', 0),
(483, 'TK-MM', 'PRB-211G', 0),
(484, 'TK-PP', 'PRB-211G', 0),
(485, 'GD-1', 'PRO-15ELR', 0),
(486, 'GD-2', 'PRO-15ELR', 0),
(487, 'TK-MM', 'PRO-15ELR', 0),
(488, 'TK-PP', 'PRO-15ELR', 0),
(489, 'GD-1', '16LTV', 0),
(490, 'GD-2', '16LTV', 0),
(491, 'TK-MM', '16LTV', 0),
(492, 'TK-PP', '16LTV', 0),
(493, 'GD-2', '5SKJ', 0),
(494, 'TK-MM', '5SKJ', 0),
(495, 'TK-PP', '5SKJ', 0),
(496, 'GD-1', 'GNC-272-SLGN', 0),
(497, 'GD-2', 'GNC-272-SLGN', 0),
(498, 'TK-MM', 'GNC-272-SLGN', 0),
(499, 'TK-PP', 'GNC-272-SLGN', 0),
(500, 'GD-1', '185SQBB', 0),
(501, 'GD-2', '185SQBB', 0),
(502, 'TK-MM', '185SQBB', 0),
(503, 'TK-PP', '185SQBB', 0),
(504, 'GD-1', '202SLC', 0),
(505, 'GD-2', '202SLC', 0),
(506, 'TK-MM', '202SLC', 0),
(507, 'TK-PP', '202SLC', 0),
(508, 'GD-1', '202RL', 0),
(509, 'GD-2', '202RL', 0),
(510, 'TK-MM', '202RL', 0),
(511, 'TK-PP', '202RL', 0),
(512, 'GD-1', '212SLK', 0),
(513, 'GD-2', '212SLK', 0),
(514, 'TK-MM', '212SLK', 0),
(515, 'TK-PP', '212SLK', 0),
(516, 'GD-1', '185SQB', 0),
(517, 'GD-2', '185SQB', 0),
(518, 'TK-MM', '185SQB', 0),
(519, 'TK-PP', '185SQB', 0),
(520, 'GD-1', 'GNB2005QBB', 0),
(521, 'GD-2', 'GNB2005QBB', 0),
(522, 'TK-MM', 'GNB2005QBB', 0),
(523, 'TK-PP', 'GNB2005QBB', 0),
(524, 'GD-1', '20FAR', 0),
(525, 'GD-2', '20FAR', 0),
(526, 'TK-MM', '20FAR', 0),
(527, 'TK-PP', '20FAR', 0),
(528, 'GD-1', '25FAR', 0),
(529, 'GD-2', '25FAR', 0),
(530, 'TK-MM', '25FAR', 0),
(531, 'TK-PP', '25FAR', 0),
(536, 'GD-1', '19M300BGS', 0),
(537, 'GD-2', '19M300BGS', 0),
(538, 'TK-MM', '19M300BGS', 0),
(539, 'TK-PP', '19M300BGS', 0),
(540, 'GD-1', '25BG', 0),
(541, 'GD-2', '25BG', 0),
(542, 'TK-MM', '25BG', 0),
(543, 'TK-PP', '25BG', 0),
(544, 'GD-1', '179NS', 0),
(545, 'GD-2', '179NS', 0),
(546, 'TK-MM', '179NS', 0),
(547, 'TK-PP', '179NS', 0),
(548, 'GD-1', '198G', 0),
(549, 'GD-2', '198G', 0),
(550, 'TK-MM', '198G', 0),
(551, 'TK-PP', '198G', 0),
(552, 'GD-1', '198G', 0),
(553, 'GD-2', '198G', 0),
(554, 'TK-MM', '198G', 0),
(555, 'TK-PP', '198G', 0),
(556, 'GD-1', '199N', 0),
(557, 'GD-2', '199N', 0),
(558, 'TK-MM', '199N', 0),
(559, 'TK-PP', '199N', 0),
(560, 'GD-1', '229S', 0),
(561, 'GD-2', '229S', 0),
(562, 'TK-MM', '229S', 0),
(563, 'TK-PP', '229S', 0),
(564, 'GD-1', '229H', 0),
(565, 'GD-2', '229H', 0),
(566, 'TK-MM', '229H', 0),
(567, 'TK-PP', '229H', 0),
(568, 'GD-1', '269H', 0),
(569, 'GD-2', '269H', 0),
(570, 'TK-MM', '269H', 0),
(571, 'TK-PP', '269H', 0),
(572, 'GD-1', '269S', 0),
(573, 'GD-2', '269S', 0),
(574, 'TK-MM', '269S', 0),
(575, 'TK-PP', '269S', 0),
(576, 'GD-1', '269N', 0),
(577, 'GD-2', '269N', 0),
(578, 'TK-MM', '269N', 0),
(579, 'TK-PP', '269N', 0),
(580, 'GD-1', '172S', 0),
(581, 'GD-2', '172S', 0),
(582, 'TK-MM', '172S', 0),
(583, 'TK-PP', '172S', 0),
(584, 'GD-1', '229N', 0),
(585, 'GD-2', '229N', 0),
(586, 'TK-MM', '229N', 0),
(587, 'TK-PP', '229N', 0),
(588, 'GD-1', '22NA', 0),
(589, 'GD-2', '22NA', 0),
(590, 'TK-MM', '22NA', 0),
(591, 'TK-PP', '22NA', 0),
(592, 'GD-1', '16GH', 0),
(593, 'GD-2', '16GH', 0),
(594, 'TK-MM', '16GH', 0),
(595, 'TK-PP', '16GH', 0),
(596, 'GD-1', '220', 0),
(597, 'GD-2', '220', 0),
(598, 'TK-MM', '220', 0),
(599, 'TK-PP', '220', 0),
(600, 'GD-1', '221', 0),
(601, 'GD-2', '221', 0),
(602, 'TK-MM', '221', 0),
(603, 'TK-PP', '221', 0),
(604, 'GD-1', '170', 0),
(605, 'GD-2', '170', 0),
(606, 'TK-MM', '170', 0),
(607, 'TK-PP', '170', 0),
(608, 'GD-1', '181', 0),
(609, 'GD-2', '181', 0),
(610, 'TK-MM', '181', 0),
(611, 'TK-PP', '181', 0),
(612, 'GD-1', '231', 0),
(613, 'GD-2', '231', 0),
(614, 'TK-MM', '231', 0),
(615, 'TK-PP', '231', 0),
(616, 'GD-1', '350', 0),
(617, 'GD-2', '350', 0),
(618, 'TK-MM', '350', 0),
(619, 'TK-PP', '350', 0),
(620, 'GD-1', '171', 0),
(621, 'GD-2', '171', 0),
(622, 'TK-MM', '171', 0),
(623, 'TK-PP', '171', 0),
(624, 'GD-1', '208', 0),
(625, 'GD-2', '208', 0),
(626, 'TK-MM', '208', 0),
(627, 'TK-PP', '208', 0),
(628, 'GD-1', '228', 0),
(629, 'GD-2', '228', 0),
(630, 'TK-MM', '228', 0),
(631, 'TK-PP', '228', 0),
(632, 'GD-1', '328', 0),
(633, 'GD-2', '328', 0),
(634, 'TK-MM', '328', 0),
(635, 'TK-PP', '328', 0),
(636, 'GD-1', '278', 0),
(637, 'GD-2', '278', 0),
(638, 'TK-MM', '278', 0),
(639, 'TK-PP', '278', 0),
(640, 'GD-1', '140L', 0),
(641, 'GD-2', '140L', 0),
(642, 'TK-MM', '140L', 0),
(643, 'TK-PP', '140L', 0),
(644, 'GD-1', '180L', 0),
(645, 'GD-2', '180L', 0),
(646, 'TK-MM', '180L', 0),
(647, 'TK-PP', '180L', 0),
(648, 'GD-1', '181L', 0),
(649, 'GD-2', '181L', 0),
(650, 'TK-MM', '181L', 0),
(651, 'TK-PP', '181L', 0),
(652, 'GD-1', '230L', 0),
(653, 'GD-2', '230L', 0),
(654, 'TK-MM', '230L', 0),
(655, 'TK-PP', '230L', 0),
(656, 'GD-1', '231L', 0),
(657, 'GD-2', '231L', 0),
(658, 'TK-MM', '231L', 0),
(659, 'TK-PP', '231L', 0),
(660, 'GD-1', '233D', 0),
(661, 'GD-2', '233D', 0),
(662, 'TK-MM', '233D', 0),
(663, 'TK-PP', '233D', 0),
(664, 'GD-1', '180-FRG', 0),
(665, 'GD-2', '180-FRG', 0),
(666, 'TK-MM', '180-FRG', 0),
(667, 'TK-PP', '180-FRG', 0),
(668, 'GD-1', '200-FRG', 0),
(669, 'GD-2', '200-FRG', 0),
(670, 'TK-MM', '200-FRG', 0),
(671, 'TK-PP', '200-FRG', 0),
(672, 'GD-1', '250-FRG', 0),
(673, 'GD-2', '250-FRG', 0),
(674, 'TK-MM', '250-FRG', 0),
(675, 'TK-PP', '250-FRG', 0),
(688, 'GD-1', '278BK', 0),
(689, 'GD-2', '278BK', 0),
(690, 'TK-MM', '278BK', 0),
(691, 'TK-PP', '278BK', 0),
(692, 'GD-1', '300RSA', 0),
(693, 'GD-2', '300RSA', 0),
(694, 'TK-MM', '300RSA', 0),
(695, 'TK-PP', '300RSA', 0),
(696, 'GD-1', '240RSA', 0),
(697, 'GD-2', '240RSA', 0),
(698, 'TK-MM', '240RSA', 0),
(699, 'TK-PP', '240RSA', 0),
(700, 'GD-1', '128-DMTS', 0),
(701, 'GD-2', '128-DMTS', 0),
(702, 'TK-MM', '128-DMTS', 0),
(703, 'TK-PP', '128-DMTS', 0),
(704, 'GD-1', '328-CF', 0),
(705, 'GD-2', '328-CF', 0),
(706, 'TK-MM', '328-CF', 0),
(707, 'TK-PP', '328-CF', 0),
(708, 'GD-1', '428-CF', 0),
(709, 'GD-2', '428-CF', 0),
(710, 'TK-MM', '428-CF', 0),
(711, 'TK-PP', '428-CF', 0),
(712, 'GD-1', '528-CF', 0),
(713, 'GD-2', '528-CF', 0),
(714, 'TK-MM', '528-CF', 0),
(715, 'TK-PP', '528-CF', 0),
(716, 'GD-1', '628-CF', 0),
(717, 'GD-2', '628-CF', 0),
(718, 'TK-MM', '628-CF', 0),
(719, 'TK-PP', '628-CF', 0),
(720, 'GD-1', '238-CF', 0),
(721, 'GD-2', '238-CF', 0),
(722, 'TK-MM', '238-CF', 0),
(723, 'TK-PP', '238-CF', 0),
(724, 'GD-1', '488-CF', 0),
(725, 'GD-2', '488-CF', 0),
(726, 'TK-MM', '488-CF', 0),
(727, 'TK-PP', '488-CF', 0),
(728, 'GD-1', '320-SKN', 0),
(729, 'GD-2', '320-SKN', 0),
(730, 'TK-MM', '320-SKN', 0),
(731, 'TK-PP', '320-SKN', 0),
(732, 'GD-1', '200-SHP', 0),
(733, 'GD-2', '200-SHP', 0),
(734, 'TK-MM', '200-SHP', 0),
(735, 'TK-PP', '200-SHP', 0),
(736, 'GD-1', '300-SHP', 0),
(737, 'GD-2', '300-SHP', 0),
(738, 'TK-MM', '300-SHP', 0),
(739, 'TK-PP', '300-SHP', 0),
(740, 'GD-1', '122-FGT', 0),
(741, 'GD-2', '122-FGT', 0),
(742, 'TK-MM', '122-FGT', 0),
(743, 'TK-PP', '122-FGT', 0),
(744, 'GD-1', '200-FGT', 0),
(745, 'GD-2', '200-FGT', 0),
(746, 'TK-MM', '200-FGT', 0),
(747, 'TK-PP', '200-FGT', 0),
(748, 'GD-1', '210-FGT', 0),
(749, 'GD-2', '210-FGT', 0),
(750, 'TK-MM', '210-FGT', 0),
(751, 'TK-PP', '210-FGT', 0),
(752, 'GD-1', '300-FGT', 0),
(753, 'GD-2', '300-FGT', 0),
(754, 'TK-MM', '300-FGT', 0),
(755, 'TK-PP', '300-FGT', 0),
(756, 'GD-1', '100-AQA', 0),
(757, 'GD-2', '100-AQA', 0),
(758, 'TK-MM', '100-AQA', 0),
(759, 'TK-PP', '100-AQA', 0),
(760, 'GD-1', '200-AQA', 0),
(761, 'GD-2', '200-AQA', 0),
(762, 'TK-MM', '200-AQA', 0),
(763, 'TK-PP', '200-AQA', 0),
(764, 'GD-1', '153F', 0),
(765, 'GD-2', '153F', 0),
(766, 'TK-MM', '153F', 0),
(767, 'TK-PP', '153F', 0),
(768, 'GD-1', '740', 0),
(769, 'GD-2', '740', 0),
(770, 'TK-MM', '740', 0),
(771, 'TK-PP', '740', 0),
(772, 'GD-1', '755', 0),
(773, 'GD-2', '755', 0),
(774, 'TK-MM', '755', 0),
(775, 'TK-PP', '755', 0),
(780, 'GD-1', '780', 0),
(781, 'GD-2', '780', 0),
(782, 'TK-MM', '780', 0),
(783, 'TK-PP', '780', 0),
(784, 'GD-1', '781', 0),
(785, 'GD-2', '781', 0),
(786, 'TK-MM', '781', 0),
(787, 'TK-PP', '781', 0),
(788, 'GD-1', '870', 0),
(789, 'GD-2', '870', 0),
(790, 'TK-MM', '870', 0),
(791, 'TK-PP', '870', 0),
(792, 'GD-1', '880', 0),
(793, 'GD-2', '880', 0),
(794, 'TK-MM', '880', 0),
(795, 'TK-PP', '880', 0),
(796, 'GD-1', '881', 0),
(797, 'GD-2', '881', 0),
(798, 'TK-MM', '881', 0),
(799, 'TK-PP', '881', 0),
(800, 'GD-1', '980', 0),
(801, 'GD-2', '980', 0),
(802, 'TK-MM', '980', 0),
(803, 'TK-PP', '980', 0),
(804, 'GD-1', '1080', 0),
(805, 'GD-2', '1080', 0),
(806, 'TK-MM', '1080', 0),
(807, 'TK-PP', '1080', 0),
(808, 'GD-1', '1480', 0),
(809, 'GD-2', '1480', 0),
(810, 'TK-MM', '1480', 0),
(811, 'TK-PP', '1480', 0),
(816, 'GD-1', '89XT', 0),
(817, 'GD-2', '89XT', 0),
(818, 'TK-MM', '89XT', 0),
(819, 'TK-PP', '89XT', 0),
(820, 'GD-1', '97DH', 0),
(821, 'GD-2', '97DH', 0),
(822, 'TK-MM', '97DH', 0),
(823, 'TK-PP', '97DH', 0),
(824, 'GD-1', '99XT', 0),
(825, 'GD-2', '99XT', 0),
(826, 'TK-MM', '99XT', 0),
(827, 'TK-PP', '99XT', 0),
(828, 'GD-1', '87DH', 0),
(829, 'GD-2', '87DH', 0),
(830, 'TK-MM', '87DH', 0),
(831, 'TK-PP', '87DH', 0),
(832, 'GD-1', '871', 0),
(833, 'GD-2', '871', 0),
(834, 'TK-MM', '871', 0),
(835, 'TK-PP', '871', 0),
(836, 'GD-1', '771', 0),
(837, 'GD-2', '771', 0),
(838, 'TK-MM', '771', 0),
(839, 'TK-PP', '771', 0),
(840, 'GD-1', '98DD', 0),
(841, 'GD-2', '98DD', 0),
(842, 'TK-MM', '98DD', 0),
(843, 'TK-PP', '98DD', 0),
(844, 'GD-1', '1280', 0),
(845, 'GD-2', '1280', 0),
(846, 'TK-MM', '1280', 0),
(847, 'TK-PP', '1280', 0),
(848, 'GD-1', 'KB001', 3),
(849, 'GD-2', 'KB001', 0),
(850, 'TK-MM', 'KB001', 0),
(851, 'TK-PP', 'KB001', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lokasi_penyimpanan`
--

CREATE TABLE `lokasi_penyimpanan` (
  `id_lokasi` varchar(20) NOT NULL,
  `nama_lokasi_penyimpanan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lokasi_penyimpanan`
--

INSERT INTO `lokasi_penyimpanan` (`id_lokasi`, `nama_lokasi_penyimpanan`) VALUES
('GD-1', 'GUDANG 1'),
('GD-2', 'GUDANG 2'),
('TK-MM', 'TOKO MAMI'),
('TK-PP', 'TOKO PAPI');

-- --------------------------------------------------------

--
-- Table structure for table `mutasi_barang`
--

CREATE TABLE `mutasi_barang` (
  `kmutasi_barang` bigint(20) NOT NULL,
  `tanggal_mutasi` date DEFAULT NULL,
  `petugas_admin` varchar(100) DEFAULT NULL,
  `petugas_lapangan` varchar(100) DEFAULT NULL,
  `id_lokasi_pengambilan` varchar(20) DEFAULT NULL,
  `id_lokasi_penyimpanan` varchar(20) DEFAULT NULL,
  `kode_barang` varchar(100) DEFAULT NULL,
  `banyak_barang` bigint(20) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mutasi_barang`
--

INSERT INTO `mutasi_barang` (`kmutasi_barang`, `tanggal_mutasi`, `petugas_admin`, `petugas_lapangan`, `id_lokasi_pengambilan`, `id_lokasi_penyimpanan`, `kode_barang`, `banyak_barang`, `keterangan`) VALUES
(2, '2018-05-05', 'Administrator', 'AHMAD', 'GD-1', 'TK-PP', 'KB001', 1, ''),
(3, '2018-05-05', 'Administrator', 'EVAN', 'GD-1', 'GD-2', 'KB001', 1, ''),
(4, '2018-05-05', 'Administrator', 'NANDI', 'TK-PP', 'TK-MM', 'KB001', 1, ''),
(5, '2018-05-05', 'Administrator', 'Agus', 'TK-MM', 'GD-1', 'KB001', 1, ''),
(6, '2018-05-05', 'Administrator', 'HENDRA', 'GD-2', 'GD-1', 'KB001', 1, '');

--
-- Triggers `mutasi_barang`
--
DELIMITER $$
CREATE TRIGGER `mutasi` AFTER INSERT ON `mutasi_barang` FOR EACH ROW BEGIN
    
    UPDATE lokasi_barang SET stok_barang = stok_barang + new.banyak_barang WHERE
    kode_barang = new.kode_barang AND id_lokasi = new.id_lokasi_penyimpanan;
   
    update lokasi_barang set stok_barang = stok_barang - new.banyak_barang where
    kode_barang = new.kode_barang and id_lokasi = new.id_lokasi_pengambilan;

    END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`kbarang_keluar`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`kbarang_masuk`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lokasi_barang`
--
ALTER TABLE `lokasi_barang`
  ADD PRIMARY KEY (`kstok_lokasi_barang`),
  ADD KEY `kode_barang` (`kode_barang`) USING BTREE;

--
-- Indexes for table `lokasi_penyimpanan`
--
ALTER TABLE `lokasi_penyimpanan`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `mutasi_barang`
--
ALTER TABLE `mutasi_barang`
  ADD PRIMARY KEY (`kmutasi_barang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `kbarang_keluar` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `kbarang_masuk` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `lokasi_barang`
--
ALTER TABLE `lokasi_barang`
  MODIFY `kstok_lokasi_barang` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=852;

--
-- AUTO_INCREMENT for table `mutasi_barang`
--
ALTER TABLE `mutasi_barang`
  MODIFY `kmutasi_barang` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lokasi_barang`
--
ALTER TABLE `lokasi_barang`
  ADD CONSTRAINT `tbl_syc` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
